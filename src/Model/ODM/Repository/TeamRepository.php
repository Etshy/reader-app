<?php
declare(strict_types=1);

namespace App\Model\ODM\Repository;

use App\Model\Interfaces\Model\TeamInterface;
use App\Model\Interfaces\Repository\TeamRepositoryInterface;
use App\Model\Persistence\Team;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\Query\Builder;
use Pagerfanta\Doctrine\MongoDBODM\QueryAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Class TeamRepository
 * @package App\Model\Repository
 */
class TeamRepository extends BaseRepository implements TeamRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Team::class);
    }

    public function findByCriteriaPaginated(array $criteria, int $page = 1): Pagerfanta
    {
        $qb = $this->createQueryBuilder();

        $qb = $this->buildCriteria($qb, $criteria);

        return $this->createPaginator($qb, $page);
    }

    private function buildCriteria(Builder $qb, array $criteria): Builder
    {
        return $qb;
    }

    private function createPaginator(Builder $query, int $page): Pagerfanta
    {
        $paginator = new Pagerfanta(new QueryAdapter($query));
        $paginator->setMaxPerPage(TeamInterface::ITEMS_NUMBER_PER_PAGE);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
