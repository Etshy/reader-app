<?php
declare(strict_types=1);

namespace App\Model\ODM\Repository;

use App\Model\Interfaces\Model\FollowInterface;
use App\Model\Interfaces\Repository\FollowRepositoryInterface;
use App\Model\Persistence\Follow;
use App\Model\Persistence\User;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\Iterator\Iterator;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Query\Builder;
use Pagerfanta\Doctrine\MongoDBODM\QueryAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Class FollowRepository
 * @package App\Model\ODM\Repository
 */
class FollowRepository extends BaseRepository implements FollowRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Follow::class);
    }

    public function getPagination(array $criteria, int $page): Pagerfanta
    {
        $qb = $this->createQueryBuilder();

        $qb = $this->buildCriteria($qb, $criteria);

        $qb->sort('createdAt', BaseRepository::SORT_DESC);

        return $this->createPaginator($qb, $page);
    }

    private function buildCriteria(Builder $qb, array $criteria): Builder
    {
        if (array_key_exists('user', $criteria) && $criteria['user'] instanceof User) {
            $qb->field('user')->references($criteria['user']);
        }
        if (array_key_exists('limit', $criteria) && isset($criteria['limit'])) {
            $qb->limit($criteria['limit']);
        }

        return $qb;
    }

    private function createPaginator(Builder $query, int $page = 1): Pagerfanta
    {
        $paginator = new Pagerfanta(new QueryAdapter($query));
        $paginator->setMaxPerPage(FollowInterface::ITEMS_NUMBER_PER_PAGE);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    /**
     * @throws MongoDBException
     */
    public function getLastsForHome(array $criteria): Iterator
    {
        $qb = $this->createQueryBuilder();
        $qb = $this->buildCriteria($qb, $criteria);
        $qb->sort('createdAt', BaseRepository::SORT_DESC);
        $qb->limit(5);

        return $qb->getQuery()->execute();
    }
}
