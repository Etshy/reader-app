<?php
declare(strict_types=1);

namespace App\Model\ODM\Repository;

use App\Exceptions\Repository\NoResultException;
use App\Model\Interfaces\Model\ChapterInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\TeamInterface;
use App\Model\Interfaces\Repository\ChapterRepositoryInterface;
use App\Model\Interfaces\Repository\RepositoryInterface;
use App\Model\Persistence\Chapter;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\Iterator\Iterator;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Query\Builder;
use Pagerfanta\Doctrine\MongoDBODM\QueryAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Class ChapterRepository
 * @package App\Model\Repository
 */
class ChapterRepository extends BaseRepository implements ChapterRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Chapter::class);
    }

    /**
     * @throws MongoDBException
     */
    public function findLatests(array $criteria): array
    {
        $qb = $this->createQueryBuilder();

        $qb->field('visible')->equals(true);

        return $qb->getQuery()->execute();
    }

    /**
     * @throws NoResultException
     */
    public function findOneByCriteria(array $criteria): ChapterInterface
    {
        $qb = $this->createQueryBuilder();
        $qb = $this->buildCriteria($qb, $criteria);

        $return = $qb->getQuery()->getSingleResult();

        if (!$return instanceof ChapterInterface) {
            throw new NoResultException();
        }

        return $return;
    }

    /**
     * @throws MongoDBException
     */
    public function findByCriteria(array $criteria): Iterator
    {
        $qb = $this->createQueryBuilder();
        $qb->setRewindable(false);
        $qb = $this->buildCriteria($qb, $criteria);

        return $qb->getQuery()->execute();
    }

    /**
     * @throws MongoDBException
     */
    public function getLatestsChaptersForTeam(TeamInterface $team, array $criteria = []): Iterator
    {
        $qb = $this->createQueryBuilder();
        //mandatory filter
        $qb->field('teams')->includesReferenceTo($team);

        $qb = $this->buildCriteria($qb, $criteria);

        $qb->sort('createdAt', self::SORT_DESC);

        return $qb->getQuery()->execute();
    }

    public function findByCriteriaPaginated(array $criteria = [], int $page = 1): Pagerfanta
    {
        $qb = $this->createQueryBuilder();

        $qb = $this->buildCriteria($qb, $criteria);

        return $this->createPaginator($qb, $page);
    }

    public function findLatestsPaginated(int $page = 1): Pagerfanta
    {
        $qb = $this->createQueryBuilder();

        $qb->field('visible')->equals(true);
        $qb->sort('publishedAt', RepositoryInterface::SORT_DESC);

        return $this->createPaginator($qb, $page);
    }

    /**
     * @throws MongoDBException
     */
    public function isChapterAlreadyExists(array $criteria): bool
    {
        $qb = $this->createQueryBuilder();
        $qb = $this->buildCriteria($qb, $criteria);

        /** @var Iterator $result */
        $result = $qb->getQuery()->execute();

        return 1 === count($result->toArray());
    }

    private function buildCriteria(Builder $qb, array $criteria): Builder
    {
        if (array_key_exists('series', $criteria)
            && isset($criteria['series'])
            && $criteria['series'] instanceof SeriesInterface
        ) {
            $qb->field('series')
                ->references($criteria['series']);
        }
        if (array_key_exists('teams', $criteria)
            && isset($criteria['teams'])
            && is_iterable($criteria['teams'])
        ) {
            $expr = $qb->expr();
            foreach ($criteria['teams'] as $team) {
                if ($team instanceof TeamInterface) {
                    $expr->field('teams')->references($team);
                }
            }
            $qb->addAnd($expr);
        }

        if (array_key_exists('number', $criteria) && isset($criteria['number'])) {
            $qb->field('number')
                ->equals($criteria['number']);
        }
        if (array_key_exists('subNumber', $criteria) && isset($criteria['subNumber'])) {
            $qb->field('subNumber')
                ->equals($criteria['subNumber']);
        }
        if (array_key_exists('visible', $criteria) && isset($criteria['visible'])) {
            $qb->field('visible')->equals($criteria['visible']);
        }
        if (array_key_exists('language', $criteria) && isset($criteria['language'])) {
            $qb->field('language')->equals($criteria['language']);
        }

        if (array_key_exists('limit', $criteria) && isset($criteria['limit'])) {
            $qb->limit($criteria['limit']);
        }

        return $qb;
    }

    private function createPaginator(Builder $query, int $page): Pagerfanta
    {
        $paginator = new Pagerfanta(new QueryAdapter($query));
        $paginator->setMaxPerPage(ChapterInterface::ITEMS_NUMBER_PER_PAGE);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
