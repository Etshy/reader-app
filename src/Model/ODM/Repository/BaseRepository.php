<?php
declare(strict_types=1);

namespace App\Model\ODM\Repository;

use App\Model\Interfaces\Model\BaseModelInterface;
use App\Model\Interfaces\Repository\RepositoryInterface;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use JetBrains\PhpStorm\Pure;

/**
 * Class BaseRepository
 * @package App\Model\Repository
 */
abstract class BaseRepository extends ServiceDocumentRepository implements RepositoryInterface
{

    /**
     * @throws MongoDBException
     */
    public function batchRemove(array $ids): object
    {
        $qb = $this->createQueryBuilder();

        return $qb->remove()
            ->field('id')
            ->in($ids)
            ->getQuery()
            ->execute();
    }

    #[Pure]
    public function getObjectManager(): DocumentManager
    {
        return $this->getDocumentManager();
    }

    /**
     * @throws MongoDBException
     */
    public function save(BaseModelInterface $model): void
    {
        if (!$model->getId()) {
            $this->getDocumentManager()->persist($model);
        }
        $this->getDocumentManager()->flush();
    }

}
