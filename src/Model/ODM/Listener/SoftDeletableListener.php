<?php
declare(strict_types=1);

namespace App\Model\ODM\Listener;

use App\Model\Interfaces\Model\SoftDeleteable;
use DateTime;
use Doctrine\Common\EventSubscriber;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Events;
use Doctrine\ODM\MongoDB\MongoDBException;

/**
 * Class SoftDeletableListener
 * @package App\Listener
 */
class SoftDeletableListener implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::preRemove,
        ];
    }

    /**
     * @throws MongoDBException
     */
    public function preRemove(LifecycleEventArgs $eventArgs): void
    {
        $document = $eventArgs->getDocument();

        if ($document instanceof SoftDeleteable) {
            $document->setDeletedAt(new DateTime());
            $eventArgs->getDocumentManager()->persist($document);
            $eventArgs->getDocumentManager()->flush();
        }
    }
}
