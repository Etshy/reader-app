<?php declare(strict_types=1);

namespace App\Model\ODM\Listener;

use App\Model\Interfaces\Model\DomainEventInterface;
use Doctrine\Bundle\MongoDBBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\Common\EventArgs;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Events;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class DomainEventCollector
 * @package App\Model\ODM\Listener
 */
class DomainEventCollector implements EventSubscriberInterface
{
    protected EventDispatcherInterface $dispatcher;
    private array $events = [];

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::preRemove,
            Events::postRemove,
        ];
    }

    public function postPersist(LifecycleEventArgs $event): void
    {
        $this->doCollect($event);
    }

    public function postUpdate(LifecycleEventArgs $event): void
    {
        $this->doCollect($event);
    }

    public function preRemove(LifecycleEventArgs $event): void
    {
        $this->doCollect($event);
    }

    public function postRemove(LifecycleEventArgs $event): void
    {
        $this->doCollect($event);
    }

    public function dispatchCollectedEvents(): void
    {
        $events = $this->events;
        $this->events = [];

        foreach ($events as $event) {
            $this->dispatcher->dispatch($event);
        }

        // Maybe listeners emitted some new events
        if ($this->events) {
            $this->dispatchCollectedEvents();
        }
    }

    public function hasUndispatchedEvents(): bool
    {
        return 0 !== count($this->events);
    }

    private function doCollect(EventArgs $event): void
    {
        $document = $event->getDocument();

        if (!$document instanceof DomainEventInterface) {
            return;
        }

        foreach ($document->popEvents() as $domainEvent) {
            // We index by object hash, not to have the same event twice
            $this->events[spl_object_hash($domainEvent)] = $domainEvent;
        }
    }

}
