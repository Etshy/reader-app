<?php declare(strict_types=1);


namespace App\Model\Interfaces\Repository;

/**
 * Interface TeamRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface TeamRepositoryInterface extends RepositoryInterface
{
}
