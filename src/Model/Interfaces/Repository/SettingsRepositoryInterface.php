<?php declare(strict_types=1);


namespace App\Model\Interfaces\Repository;

/**
 * Interface SettingsRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface SettingsRepositoryInterface extends RepositoryInterface
{
}
