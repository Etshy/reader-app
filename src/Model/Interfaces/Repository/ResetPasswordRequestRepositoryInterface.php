<?php declare(strict_types=1);

namespace App\Model\Interfaces\Repository;

use App\Model\Interfaces\Model\ResetPasswordRequestInterface;
use App\Model\Interfaces\Model\UserInterface;
use DateTimeInterface;

/**
 *
 */
interface ResetPasswordRequestRepositoryInterface extends RepositoryInterface
{
    public function removeExpiredResetPasswordRequests(): void;

    public function saveResetPasswordRequest(ResetPasswordRequestInterface $model): void;

    public function getMostRecentNonExpiredRequestDate(UserInterface $user): ?DateTimeInterface;
}

