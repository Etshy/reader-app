<?php
declare(strict_types=1);


namespace App\Model\Interfaces\Repository;

/**
 * Interface ImageRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface ImageRepositoryInterface extends FileRepositoryInterface
{
}
