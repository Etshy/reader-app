<?php
declare(strict_types=1);


namespace App\Model\Interfaces\Repository;

use Doctrine\ODM\MongoDB\Iterator\Iterator;

/**
 * Interface ChapterNotificationRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface ChapterNotificationRepositoryInterface extends NotificationRepositoryInterface
{
    public function findByCriteria(array $criteria): Iterator;
}
