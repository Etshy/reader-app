<?php declare(strict_types=1);


namespace App\Model\Interfaces\Repository;

use App\Model\Criteria\SeriesCriteria;
use Pagerfanta\Pagerfanta;

/**
 * Interfaces SeriesRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface SeriesRepositoryInterface extends RepositoryInterface
{
    public function findByCriteria(SeriesCriteria $criteria): array;

    public function findByCriteriaPaginated(SeriesCriteria $criteria, int $page): Pagerfanta;
}
