<?php declare(strict_types=1);


namespace App\Model\Interfaces\Model;

/**
 * Interface ReaderSettingsTraitInterface
 * @package App\Model\Interfaces\Model
 */
interface ReaderSettingsTraitInterface
{
    public function getReaderMode(): ?string;

    public function setReaderMode(?string $readerMode): void;
}
