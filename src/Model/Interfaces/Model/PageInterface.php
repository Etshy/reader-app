<?php declare(strict_types=1);


namespace App\Model\Interfaces\Model;

use App\Model\Interfaces\Model\Files\ImageInterface;


/**
 * Interface PageInterface
 * @package App\Model\Interfaces\Model
 */
interface PageInterface
{
    public function getOrder(): int;

    public function setOrder(int $order): void;

    public function getImage(): ImageInterface;

    public function setImage(ImageInterface $image): void;

    public function getScrambledImage(): ?ImageInterface;

    public function setScrambledImage(?ImageInterface $scrambledImage): void;
}
