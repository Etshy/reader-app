<?php declare(strict_types=1);


namespace App\Model\Interfaces\Model;

/**
 * Interface ChapterReadenInterface
 * @package App\Model\Interfaces\Model
 */
interface ChapterReadenInterface extends BaseModelInterface
{
    const ITEMS_NUMBER_PER_PAGE = 5;

    public function getChapter(): ChapterInterface;

    public function setChapter(ChapterInterface $chapter): void;

    public function getSeries(): SeriesInterface;

    public function setSeries(SeriesInterface $series): void;

    public function getUser(): UserInterface;

    public function setUser(UserInterface $user): void;
}
