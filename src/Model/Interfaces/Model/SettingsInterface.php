<?php declare(strict_types=1);


namespace App\Model\Interfaces\Model;

use App\Model\Interfaces\Model\Files\ImageInterface;

/**
 * Interface SettingsInterface
 * @package App\Model\Interfaces\Model
 */
interface SettingsInterface extends BaseModelInterface
{
    public function getDefaultReaderMode(): ?string;

    public function setDefaultReaderMode(?string $defaultReaderMode): void;

    public function getDefaultReaderDirection(): ?string;

    public function setDefaultReaderDirection(?string $defaultReaderDirection): void;

    public function isDownloadEnabled(): bool;

    public function setDownloadEnabled(bool $downloadEnabled): void;

    public function isDownloadProtected(): bool;

    public function setDownloadProtected(bool $downloadProtected): void;

    public function setLogo(?ImageInterface $logo): void;

    public function getLogo(): ?ImageInterface;
}
