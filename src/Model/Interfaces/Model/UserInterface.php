<?php declare(strict_types=1);

namespace App\Model\Interfaces\Model;

use App\Model\Interfaces\Model\Files\ImageInterface;
use DateTime;
use Doctrine\Common\Collections\Collection;

/**
 * Interfaces UserInterface
 * @package App\Model\Interfaces\Model
 */
interface UserInterface extends BaseModelInterface
{
    const PRIVACY_ALL = 'all';
    const PRIVACY_FRIENDS = 'friends';
    const PRIVACY_PRIVATE = 'private';

    const ITEMS_NUMBER_PER_PAGE = 25;

    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    public function setUsername(string $username): void;

    public function setPassword(string $password): void;

    public function setLastLogin(DateTime $lastLogin): void;

    public function getLastLogin(): ?DateTime;

    public function setEmail(?string $email): void;

    public function getEmail(): ?string;

    public function setLanguage(?string $language): void;

    public function getLanguage(): ?string;

    public function getFirstname(): ?string;

    public function setFirstname(?string $firstname): void;

    public function getLastname(): ?string;

    public function setLastname(?string $lastname): void;

    public function getBiography(): ?string;

    public function setBiography(?string $biography): void;

    public function getAvatar(): ImageInterface|null;

    public function setAvatar(ImageInterface $avatar): void;

    public function isPrivate(): bool;

    public function setPrivate(bool $private): void;

    public function getWebsite(): ?string;

    public function setWebsite(?string $website): void;

    public function getDiscordProfile(): ?string;

    public function setDiscordProfile(?string $discordProfile): void;

    public function getTeams(): Collection;

    public function setTeams(Collection $teams): void;

    public function addTeam(TeamInterface $team): void;

    public function getSettings(): ?UserSettingsInterface;

    public function setSettings(UserSettingsInterface $settings): void;

    public function setUsernameCanonical(string $canonicalize): void;

}
