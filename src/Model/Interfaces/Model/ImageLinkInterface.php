<?php
declare(strict_types=1);


namespace App\Model\Interfaces\Model;

/**
 * Interface ImageLinkInterface
 * @package App\Model\Interfaces\Model
 */
interface ImageLinkInterface
{
    const SOURCE_LOCAL = 'local';
    //add others sources (images host)
}
