<?php
declare(strict_types=1);

namespace App\Model\Persistence\Embed;

use App\Model\Interfaces\Model\ReaderSettingsInterface;
use App\Model\Interfaces\Model\UserSettingsInterface;
use App\Model\Persistence\Traits\ReaderSettingsTrait;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

/**
 * Class UserSettings
 * @package App\Model\Persistence
 */
class UserSettings implements JsonSerializable, ReaderSettingsInterface, UserSettingsInterface
{
    use ReaderSettingsTrait;

    protected ?string $displayLanguage;
    protected bool $chapterEmailNotificationActivated;

    public function __construct()
    {
        $this->chapterEmailNotificationActivated = false;
    }

    #[Pure]
    #[ArrayShape(['displayLanguage' => "null|string", 'chapterEmailNotificationActivated' => "bool", 'readerMode' => "null|string", 'readerDirection' => "null|string"])]
    public function jsonSerialize(): array
    {
        return [
            'displayLanguage' => $this->getDisplayLanguage(),
            'chapterEmailNotificationActivated' => $this->isChapterEmailNotificationActivated(),
            'readerMode' => $this->getReaderMode(),
            'readerDirection' => $this->getReaderDirection(),
        ];
    }

    public function getDisplayLanguage(): ?string
    {
        return $this->displayLanguage;
    }

    public function setDisplayLanguage(?string $displayLanguage): void
    {
        $this->displayLanguage = $displayLanguage;
    }

    public function isChapterEmailNotificationActivated(): bool
    {
        return $this->chapterEmailNotificationActivated;
    }

    public function setChapterEmailNotificationActivated(bool $chapterEmailNotificationActivated): void
    {
        $this->chapterEmailNotificationActivated = $chapterEmailNotificationActivated;
    }
}
