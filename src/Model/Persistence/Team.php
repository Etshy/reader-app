<?php
declare(strict_types=1);

namespace App\Model\Persistence;

use App\Form\Backend\Team\TeamForm;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Model\SluggableInterface;
use App\Model\Interfaces\Model\TeamInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\PersistentCollection;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Team
 * @package App\Model\Persistence
 */
class Team extends BaseModel implements TeamInterface, SluggableInterface
{
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    protected ?string $name;
    #[Assert\Type('string')]
    #[Assert\NotBlank(allowNull: true)]
    protected ?string $website = null;
    #[Assert\Type('string')]
    #[Assert\NotBlank(allowNull: true)]
    protected ?string $irc = null;
    #[Assert\Type('string')]
    #[Assert\NotBlank(allowNull: true)]
    protected ?string $discord = null;
    #[Assert\Type('string')]
    #[Assert\NotBlank(allowNull: true)]
    protected ?string $twitter = null;
    #[Assert\Type('string')]
    #[Assert\NotBlank(allowNull: true)]
    protected ?string $facebook = null;

    /**
     * Can't delete the Asset\All yet
     *
     * @Assert\All({
     *     @Assert\Type(type="App\Model\Persistence\User"),
     * })
     */
    #[Assert\Valid]
    protected ?Collection $members = null;

    /**
     * @var Collection|null
     * @Assert\All({
     *     @Assert\Type(type="App\Model\Persistence\User")
     * })
     */
    #[Assert\Valid]
    protected ?Collection $managers = null;
    protected ?Collection $chapters;
    protected ?string $slug = null;
    protected ?Collection $series;
    protected ?ImageInterface $image = null;

    #[Pure]
    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->managers = new ArrayCollection();
        $this->chapters = new ArrayCollection();
    }

    #[Assert\Callback]
    public function validate(
        ExecutionContextInterface $context
    ): void {
        //Managers validation
        if ($this->getManagers()) {
            foreach ($this->getManagers() as $manager) {
                //if the $manager is not in members, that's a violation
                if (!$this->getMembers()?->contains($manager)) {
                    $context->buildViolation('admin.team.form.managers.not-in-members')
                        ->atPath(TeamForm::MANAGERS)
                        ->addViolation();
                }
            }
        }
    }

    public function getManagers(): ?Collection
    {
        return $this->managers;
    }

    public function setManagers(?Collection $managers): void
    {
        $this->managers = $managers;
    }

    public function getMembers(): ?Collection
    {
        return $this->members;
    }

    public function setMembers(?Collection $members): void
    {
        if ($this->members instanceof PersistentCollection) {
            foreach ($this->members->getSnapshot() as $user) {
                if (!$user instanceof User) {
                    continue;
                }
                $user->getTeams()->removeElement($this);
            }
        }

        $this->members = $members;

        foreach ($this->members as $member) {
            $member->addTeam($this);
        }
    }

    public function getChapters(): ?Collection
    {
        return $this->chapters;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function getSeries(): ?Collection
    {
        return $this->series;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): void
    {
        $this->website = $website;
    }

    public function getIrc(): ?string
    {
        return $this->irc;
    }

    public function setIrc(?string $irc): void
    {
        $this->irc = $irc;
    }

    public function getDiscord(): ?string
    {
        return $this->discord;
    }

    public function setDiscord(?string $discord): void
    {
        $this->discord = $discord;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): void
    {
        $this->twitter = $twitter;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): void
    {
        $this->facebook = $facebook;
    }

    public function getImage(): ?ImageInterface
    {
        return $this->image;
    }

    public function setImage(?ImageInterface $image): Team
    {
        $this->image = $image;

        return $this;
    }
}
