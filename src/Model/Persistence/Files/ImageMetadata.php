<?php
declare(strict_types=1);


namespace App\Model\Persistence\Files;

/**
 * Class ImageMetadata
 * @package App\Model\Persistence\Files
 */
class ImageMetadata extends FileMetadata
{
    protected int $width;
    protected int $height;
    protected string $type = 'image';

    public function getWidth(): int
    {
        return $this->width;
    }

    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function setHeight(int $height): void
    {
        $this->height = $height;
    }
}
