<?php
declare(strict_types=1);

namespace App\Model\Persistence;

use App\Model\Interfaces\Model\ChapterInterface;
use App\Model\Interfaces\Model\ChapterReadenInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\UserInterface;

/**
 * Class ChapterReaden
 * @package App\Model\Persistence
 */
class ChapterReaden extends BaseModel implements ChapterReadenInterface
{
    protected ChapterInterface $chapter;
    protected SeriesInterface $series;
    protected UserInterface $user;

    public function getChapter(): ChapterInterface
    {
        return $this->chapter;
    }

    public function setChapter(ChapterInterface $chapter): void
    {
        $this->chapter = $chapter;
    }

    public function getSeries(): SeriesInterface
    {
        return $this->series;
    }

    public function setSeries(SeriesInterface $series): void
    {
        $this->series = $series;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): void
    {
        $this->user = $user;
    }
}
