<?php declare(strict_types=1);

namespace App\Model\Persistence\Traits;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class DomainEventTrait
 * @package App\Model\Persistence\Traits
 */
trait DomainEventTrait
{
    protected array $events = [];

    public function popEvents(): array
    {
        $events = $this->events;

        $this->events = [];

        return $events;
    }

    public function raise(Event $event):void
    {
        $this->events[] = $event;
    }
}
