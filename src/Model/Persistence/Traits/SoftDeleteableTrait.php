<?php declare(strict_types=1);

namespace App\Model\Persistence\Traits;

use DateTime;

/**
 * Class DeleteteableTrait
 * @package App\Model\Persistence\Traits
 */
trait SoftDeleteableTrait
{
    protected ?DateTime $deletedAt = null;

    public function getDeletedAt(): ?DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?DateTime $deletedAt = null): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function isDeleted(): bool
    {
        return null !== $this->deletedAt;
    }
}
