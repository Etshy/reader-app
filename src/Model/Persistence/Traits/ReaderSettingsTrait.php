<?php
declare(strict_types=1);

namespace App\Model\Persistence\Traits;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ReaderSettingsTrait
 * @package App\Model\Persistence\Traits
 */
trait ReaderSettingsTrait
{
    #[Assert\Type('string')]
    protected ?string $readerMode = null;

    #[Assert\Type('string')]
    protected ?string $readerDirection = null;

    public function getReaderMode(): ?string
    {
        return $this->readerMode;
    }

    public function setReaderMode(?string $readerMode): void
    {
        $this->readerMode = $readerMode;
    }

    public function getReaderDirection(): ?string
    {
        return $this->readerDirection;
    }

    public function setReaderDirection(?string $readerDirection): void
    {
        $this->readerDirection = $readerDirection;
    }
}
