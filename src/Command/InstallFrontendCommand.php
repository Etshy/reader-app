<?php declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Process;

class InstallFrontendCommand extends Command
{
    protected static $defaultName = 'bookuto:install-frontend';

    protected static $defaultDescription = 'Execute all commands to install the frontend and build the assets.';
    protected string $rootPath;

    public function __construct(string $rootPath)
    {
        parent::__construct();
        $this->rootPath = $rootPath;
    }


    protected function configure():void
    {
        $this
            ->setHelp('This command will execute all the necessary tasks to install and build the assets.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        //Create and run all the process to install frontend
        $questionHelper = $this->getHelper('question');

        // Ask for the directory where the reader is located (inside the server folder) to change the webpack.config.js (replace /bookuto)
        $question = new Question('<question>Please enter the path from your server root folder to the reader (default: `/bookuto`) : </question>', '/bookuto');
        $path = $questionHelper->ask($input, $output, $question);

        //npm install --ignore-scripts
        $output->writeln('<info>Running `npm install`. This can take a few minutes...</info>');

        $installProcess = new Process(['npm', 'install']); // not install --production because we need to build the assets and so need dev deps
        $installProcess->setWorkingDirectory($this->rootPath);
        $installProcess->setTimeout(9999);
        $installProcess->run();
        if (!$installProcess->isSuccessful()) {
            $output->writeln('<error>Error running `npm install`</error>');
            $output->writeln('<error>'.$installProcess->getErrorOutput().'</error>');
            return Command::FAILURE;
        }
        echo $installProcess->getOutput();

        $output->writeln('<info>`npm install` successfully done</info>');


        //npx gulp build
        $output->writeln('<info>Running `npx gulp build`. This can take a few minutes...</info>');
        $gulpProcess = new Process(['npx', 'gulp', 'build']);
        $gulpProcess->setTimeout(9999);
        $gulpProcess->setWorkingDirectory($this->rootPath . '/assets/fomantic');
        $gulpProcess->run(function ($type, $buffer) {
            if (Process::ERR !== $type) {
                echo $buffer;
            }
        });
        if (!$gulpProcess->isSuccessful()) {
            $output->writeln('<error>Error running `npx gulp build`</error>');
            $output->writeln('<error>'.$gulpProcess->getErrorOutput().'</error>');
            return Command::FAILURE;
        }
        echo $gulpProcess->getOutput();
        $output->writeln('<info>`npx gulp build` successfully done</info>');

        $fileFinder = new Finder();
        $fileFinder->depth(0)->files()->name('/^(webpack.config.js)$/')->in($this->rootPath);

        foreach ($fileFinder as $file) {
            $contents = $file->getContents();
            file_put_contents($file->getRealPath(), str_replace('/bookuto', $path, $contents));
        }
        $output->writeln('<info>Output directory for assets changed</info>');

        //npm run encore production
        $output->writeln('<info>Running `npm run encore production`. This can take a few minutes...</info>');
        $encoreProcess = new Process(['npm', 'run', 'encore', 'production']);
        $encoreProcess->setTimeout(9999);
        $encoreProcess->setWorkingDirectory($this->rootPath);
        $encoreProcess->run(function ($type, $buffer) {
            if (Process::ERR !== $type) {
                echo $buffer;
            }
        });
        if (!$encoreProcess->isSuccessful()) {
            $output->writeln('<error>Error running `npm run encore production`</error>');
            $output->writeln('<error>'.$installProcess->getErrorOutput().'</error>');
            return Command::FAILURE;
        }

        //DONE
        $output->writeln('<info>Frontend installation done!</info>');

        return Command::SUCCESS;
    }


}
