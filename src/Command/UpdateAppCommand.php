<?php declare(strict_types=1);

namespace App\Command;

use App\Utils\AppUpdater\Exception\SourceZipNotAvailableExeption;
use App\Utils\AppUpdater\Exception\VersionMalformedException;
use App\Utils\AppUpdater\Updater as AppUpdater;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class UpdateAppCommand extends Command
{
    protected static $defaultName = 'bookuto:update-app';

    protected static $defaultDescription = 'Execute all commands to install the frontend and build the assets.';
    protected AppUpdater $appUpdater;

    public function __construct(AppUpdater $appUpdater)
    {
        $this->appUpdater = $appUpdater;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp('This command will check if an update is available and update the app.');
    }

    /**
     * @throws TransportExceptionInterface
     * @throws VersionMalformedException
     * @throws SourceZipNotAvailableExeption
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // TODO: call Updater to check update donwload it, etc.
        $this->appUpdater->setOutput($output);

        $this->appUpdater->update();

        return self::SUCCESS;
    }

}
