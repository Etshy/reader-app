<?php declare(strict_types=1);

namespace App\DependencyInjection\CompilerPass;

use App\Exceptions\DependencyInjection\InvalidConfigurationException;
use App\Model\Interfaces\Repository\RepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\ServiceRepositoryCompilerPass as ServiceRepositoryORMCompilerPass;
use Doctrine\Bundle\MongoDBBundle\DependencyInjection\Compiler\ServiceRepositoryCompilerPass as ServiceRepositoryODMCompilerPass;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ReflectionClass;
use ReflectionException;
use RegexIterator;
use SplFileInfo;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class DoctrineRepositoryCompilerPass
 * @package App\DependencyInjection\CompilerPass
 */
class DoctrineRepositoryCompilerPass implements CompilerPassInterface
{
    private const USE_ODM = 'mongodb';
    private const USE_ORM = 'orm';

    private static array $doctrineDrivers = [
        'orm' => [
            'registry' => 'doctrine',
            'tag' => 'doctrine.event_subscriber',
        ],
        'mongodb' => [
            'registry' => 'doctrine_mongodb',
            'tag' => 'doctrine_mongodb.odm.event_subscriber',
        ],
    ];

    private ContainerBuilder $container;

    /**
     * @throws InvalidConfigurationException
     */
    public function process(ContainerBuilder $container): void
    {
        $this->container = $container;

        $dbDriver = $this->getDbDriver();

        if (!in_array($dbDriver, [self::USE_ODM, self::USE_ORM])) {
            throw new InvalidConfigurationException('Unknown dbdriver : '.$dbDriver);
        }

        $container->setAlias('app.doctrine_registry', new Alias(self::$doctrineDrivers[$dbDriver]['registry'], false));

        $container->setParameter('app.backend_type_'.$dbDriver, true);

        if (isset(self::$doctrineDrivers[$dbDriver])) {
            $definition = $container->getDefinition('object_manager');
            $definition->setFactory([new Reference('app.doctrine_registry'), 'getManager']);
        }

        $this->loadRepository();
    }

    private function getDbDriver(): string
    {
        //get the param to know which Doctrine we use
        return $this->container->resolveEnvPlaceholders(
            $this->container->getParameter('db_driver'),
            true // Resolve to actual values
        );
    }

    private function loadRepository(): void
    {
        $dbDriver = $this->getDbDriver();

        $rootSrcDir = $this->getRootSrcDir();

        switch ($dbDriver) {
            case self::USE_ORM:
                $rootSrcDir .= '/Model/ORM/Repository';
                $namespace = 'App\\Model\\ORM\\Repository\\';
                break;

            case self::USE_ODM:
            default:
                $rootSrcDir .= '/Model/ODM/Repository';
                $namespace = 'App\\Model\\ODM\\Repository\\';
                break;
        }
        //search for the Repository files
        $allFiles = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootSrcDir));
        $phpFiles = new RegexIterator($allFiles, '/\.php$/');
        $definitions = [];
        foreach ($phpFiles as $phpFile) {
            if (!$phpFile instanceof SplFileInfo) {
                continue;
            }
            $filename = $phpFile->getFilename();
            $className = str_replace('.php', '', $filename);
            try {
                $reflectionClass = new ReflectionClass($namespace.$className);
            } catch (ReflectionException) {
                //not a class
                continue;
            }
            if (!$reflectionClass->implementsInterface(RepositoryInterface::class)) {
                //not implementing the required Interface
                continue;
            }
            $classImplements = $reflectionClass->getInterfaceNames();
            $abstractRepositoryInterfaceNamespace = 'App\\Model\\Interfaces\\Repository\\';
            //Check if  the Interface for current Repository class is implemented
            if (in_array($abstractRepositoryInterfaceNamespace.$reflectionClass->getShortName().'Interface', $classImplements)) {
                //Create the Definition to add the Repository in the Container
                $definition = new Definition($reflectionClass->getName());
                //Add the Doctrine Object Manager (Document or Entity) to the Repository construct.
                if ($dbDriver === self::USE_ODM) {
                    $definition->addArgument(new Reference('doctrine_mongodb'));
                    $definition->addTag(ServiceRepositoryODMCompilerPass::REPOSITORY_SERVICE_TAG);
                } elseif ($dbDriver === self::USE_ORM) {
                    $definition->addArgument(new Reference('doctrine.orm.entity_manager'));
                    $definition->addTag(ServiceRepositoryORMCompilerPass::REPOSITORY_SERVICE_TAG);
                }
                //Service is not public, we can't get it with $container->get()
                $definition->setPublic(false);
                $definition->setAutowired(true);
                $definition->setAutoconfigured(true);

                //Add the definition with FQCN of Repo
                $definitions[$namespace.$className] = $definition;
                //And add the definition with the Interface as key to be able to DI with Interface
                $definitions[$abstractRepositoryInterfaceNamespace.$reflectionClass->getShortName().'Interface'] = $definition;

            }
        }

        //Add all the definitions
        $this->container->addDefinitions($definitions);
    }

    private function getRootSrcDir(): string
    {
        //get the root src dir
        return $this->getProjectDir().'/src';
    }

    private function getProjectDir(): array|string
    {
        //get the root src dir
        return $this->container->resolveEnvPlaceholders(
            $this->container->getParameter('kernel.project_dir'),
            true // Resolve to actual values
        );
    }
}
