<?php
declare(strict_types=1);


namespace App\Security;

use App\Model\Interfaces\Model\UserInterface;
use App\Service\UserService;
use DateTime;
use Exception;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class InteractiveLoginListener
 * @package App\Security
 */
class InteractiveLoginListener
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @throws Exception
     */
    public function onSecurityInteractivelogin(InteractiveLoginEvent $interactiveLoginEvent): void
    {
        $username = $interactiveLoginEvent->getAuthenticationToken()->getUser()->getUserIdentifier();
        $user = $this->userService->findUserByUsername($username);
        if ($user instanceof UserInterface) {
            $user->setLastLogin(new DateTime());
            $this->userService->save($user);
        }
    }
}
