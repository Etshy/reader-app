<?php declare(strict_types=1);

namespace App\Security\ResetPassword\Model;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use LogicException;
use RuntimeException;

class PublicResetToken
{
    private ?string $token;
    private DateTimeInterface $expiresAt;
    private DateTimeInterface $generatedAt;

    public function __construct(string $token, DateTimeInterface $expiresAt, DateTimeInterface $generatedAt = null)
    {
        $this->token = $token;
        $this->expiresAt = $expiresAt;
        $this->generatedAt = $generatedAt;
    }

    /**
     * Returns the full token the user should use.
     *
     * Internally, this consists of two parts - the selector and
     * the hashed token - but that's an implementation detail
     * of how the token will later be parsed.
     */
    public function getToken(): string
    {
        if (null === $this->token) {
            throw new RuntimeException('The token property is not set. Calling getToken() after calling clearToken() is not allowed.');
        }

        return $this->token;
    }

    /**
     * Allow the token object to be safely persisted in a session.
     */
    public function clearToken(): void
    {
        $this->token = null;
    }

    public function getExpiresAt(): DateTimeInterface
    {
        return $this->expiresAt;
    }

    /**
     * Get the interval that the token is valid for.
     *
     * @throws LogicException
     *
     * @psalm-suppress PossiblyFalseArgument
     */
    public function getExpiresAtIntervalInstance(): DateInterval
    {
        if (null === $this->generatedAt) {
            throw new LogicException(sprintf('%s initialized without setting the $generatedAt timestamp.', self::class));
        }

        $createdAtTime = DateTimeImmutable::createFromFormat('U', (string)$this->generatedAt);

        return $this->expiresAt->diff($createdAtTime);
    }
}
