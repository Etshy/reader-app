<?php declare(strict_types=1);

namespace App\Security\ResetPassword\DependencyInjection;

use Exception;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class ResetPasswordExtension extends Extension
{
    /**
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new ResetPasswordConfiguration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('reset_password.lifetime', $config['lifetime']);
        $container->setParameter('reset_password.throttle_limit', $config['throttle_limit']);
    }
}
