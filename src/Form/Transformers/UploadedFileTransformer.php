<?php
declare(strict_types=1);

namespace App\Form\Transformers;

use App\Model\Interfaces\Model\Files\FileInterface;
use App\Service\FileService;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class UploadedFileTransformer
 * @package App\Form\Transformers
 */
class UploadedFileTransformer implements DataTransformerInterface
{

    private FileService $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * @param mixed $value
     *
     * @return null
     */
    public function transform($value)
    {
        //return null, no need to return something for a file input
        return null;
    }

    /**
     * Transform an uploadedFile into a File or an Image
     *
     * @param $value
     *
     * @return FileInterface|null
     */
    public function reverseTransform($value): ?FileInterface
    {
        return $this->fileService->createFileFromUploadedFile($value);
    }
}
