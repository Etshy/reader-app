<?php
declare(strict_types=1);


namespace App\Form\Frontend;

use App\Application\Series\StatusManager;
use App\Application\Series\TagsManager;
use App\Controller\Query\SeriesFiltersQuery;
use App\Model\Criteria\SeriesCriteria;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class SeriesFiltersForm
 * @package App\Form\Frontend
 */
class SeriesFiltersForm extends AbstractType
{
    public const INCLUDE_TAGS = 'include_tags';
    public const EXCLUDE_TAGS = 'exclude_tags';
    public const INCLUSION_MODE = 'inclusion_mode';
    public const STATUS = 'status';

    protected TagsManager $tagsManager;
    protected StatusManager $statusManager;
    protected TranslatorInterface $translator;

    public function __construct(TagsManager $tagsManager, StatusManager $statusManager, TranslatorInterface $translator)
    {
        $this->tagsManager = $tagsManager;
        $this->statusManager = $statusManager;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var SeriesFiltersQuery $data */
        $data = $options['data'];
        $builder
            ->setMethod('GET')
            ->add(self::INCLUDE_TAGS, ChoiceType::class, [
                'choices' => $this->tagsManager->getTagsForForm(),
                'placeholder' => 'label.tags.choose',
                'label' => 'label.include_tags.label',
                'multiple' => true,
                'attr' => [
                    'class' => 'ui inverted search dropdown',
                ],
                'data' => $data->getIncludeTags(),
            ])
            ->add(self::EXCLUDE_TAGS, ChoiceType::class, [
                'choices' => $this->tagsManager->getTagsForForm(),
                'placeholder' => 'label.tags.choose',
                'label' => 'label.exclude_tags.label',
                'multiple' => true,
                'attr' => [
                    'class' => 'ui inverted search dropdown',
                ],
                'data' => $data->getExcludeTags(),
            ])
            ->add(self::INCLUSION_MODE, ChoiceType::class, [
                'choices' => [
                    $this->translator->trans('label.inclusion_mode.all') => SeriesCriteria::INCLUSION_MODE_ALL,
                    $this->translator->trans('label.inclusion_mode.any') => SeriesCriteria::INCLUSION_MODE_ANY,
                ],
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
            ])
            ->add(
                self::STATUS,
                ChoiceType::class,
                [
                    'choices' => $this->statusManager->getStatusForForm(),
                    'placeholder' => 'label.status.choose',
                    'label' => 'label.status.label',
                    'attr' => [
                        'class' => 'ui inverted clearable selection dropdown',
                    ],
                    'data' => $data->getStatus(),
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('csrf_protection', false);
        $resolver->setDefault('data', new SeriesFiltersQuery());
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
