<?php
declare(strict_types=1);

namespace App\Form\Frontend;

use App\Model\Interfaces\Model\PageInterface;
use App\Model\Persistence\Chapter;
use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ReaderForm
 * @package App\Form\Frontend
 */
class ReaderForm extends AbstractType
{
    public const CHAPTER = 'chapter';
    public const PAGE = 'page';

    /**
     * @var TranslatorInterface
     */
    private TranslatorInterface $translator;

    /**
     * ReaderForm constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(self::CHAPTER, DocumentType::class, [
                'class' => Chapter::class,
                'choices' => $options['chapters'],
                'choice_label' => function (Chapter $chapter) {
                    if ($chapter->getSeries() && $chapter->getSeries()->getCustomTitle()) {
                        $return = $chapter->getSeries()->getCustomTitle();
                    } else {
                        $return = $this->translator->trans('label.chapter');
                    }
                    $return .= ' '.$chapter->getNumber();
                    return $return;
                },
                'choice_attr' => function (Chapter $chapter) {
                    return [
                        'data-number' => $chapter->getNumber(),
                    ];
                },
                'attr' => [
                    'class' => 'ui inverted dropdown tiny',
                ],
                'required' => false,
                'placeholder' => false,
                'label' => 'label.chapter',
            ])
            ->add(self::PAGE, ChoiceType::class, [
                'choices' => $options['pages'],
                'choice_label' => function (PageInterface $page) {
                    $return = $this->translator->trans('label.page');
                    return $return.' '.($page->getOrder());
                },
                'attr' => [
                    'class' => 'ui inverted dropdown tiny',
                ],
                'required' => false,
                'placeholder' => false,
                'label' => 'label.page',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'chapters' => null,
            'pages' => null,
        ]);
    }
}
