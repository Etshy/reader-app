<?php
declare(strict_types=1);

namespace App\Form\Types;

use App\Form\Transformers\UploadedFileTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class UploadedFileType
 * @package App\Form\Types
 */
class UploadedFileType extends AbstractType
{

    private UploadedFileTransformer $transformer;

    public function __construct(UploadedFileTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this->transformer);
    }

    public function getParent(): string
    {
        return FileType::class;
    }
}
