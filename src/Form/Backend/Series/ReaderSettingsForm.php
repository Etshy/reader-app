<?php
declare(strict_types=1);

namespace App\Form\Backend\Series;

use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Persistence\Embed\ReaderSettings;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReaderSettingsForm
 * @package App\Form\Backend
 */
class ReaderSettingsForm extends AbstractType
{
    public const READER_MODE = 'readerMode';
    public const READER_DIRECTION = 'readerDirection';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = false;
        if (array_key_exists('disabled', $options)) {
            $disabled = $options['disabled'];
        }

        $builder
            ->add(self::READER_MODE, ChoiceType::class, [
                'choices' => [
                    'label.reader_mode.classic' => SeriesInterface::READER_MODE_CLASSIC_VALUE,
                    'label.reader_mode.webtoon' => SeriesInterface::READER_MODE_WEBTOON_VALUE,
                ],
                'required' => false,
                'placeholder' => 'label.reader_mode.choose',
                'label' => 'label.reader_mode.label',
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
                'disabled' => $disabled,
            ])
            ->add(self::READER_DIRECTION, ChoiceType::class, [
                'choices' => [
                    'label.reader_direction.ltr' => SeriesInterface::READER_DIRECTION_LTR_VALUE,
                    'label.reader_direction.rtl' => SeriesInterface::READER_DIRECTION_RTL_VALUE,
                ],
                'required' => false,
                'placeholder' => 'label.reader_direction.choose',
                'label' => 'label.reader_direction.label',
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ReaderSettings::class,
        ]);
    }
}
