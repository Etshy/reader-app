<?php
declare(strict_types=1);

namespace App\Form\Backend\Team;

use App\Form\Types\UploadedFileType;
use App\Model\Interfaces\Repository\RepositoryInterface;
use App\Model\ODM\Repository\UserRepository;
use App\Model\Persistence\User;
use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class AddTeamForm
 * @package App\Form\Backend\Team
 */
class TeamForm extends AbstractType
{
    public const NAME = 'name';
    public const IMAGE = 'image';
    public const WEBSITE = 'website';
    public const IRC = 'irc';
    public const DISCORD = 'discord';
    public const TWITTER = 'twitter';
    public const FACEBOOK = 'facebook';
    public const VISIBLE = 'visible';
    public const MEMBERS = 'members';
    public const MANAGERS = 'managers';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', HiddenType::class)
            ->add(self::NAME, TextType::class, [
                'required' => true,
                'label' => 'label.name',
            ])
            ->add(self::IMAGE, UploadedFileType::class, [
                'required' => false,
                'label' => 'label.image',
            ])
            ->add(self::WEBSITE, TextType::class, [
                'required' => false,
                'label' => 'label.website',
            ])
            ->add(self::IRC, TextType::class, [
                'required' => false,
                'label' => 'label.irc',
            ])
            ->add(self::DISCORD, TextType::class, [
                'required' => false,
                'label' => 'label.discord',
            ])
            ->add(self::TWITTER, TextType::class, [
                'required' => false,
                'label' => 'label.twitter',
            ])
            ->add(self::FACEBOOK, TextType::class, [
                'required' => false,
                'label' => 'label.facebook',
            ])
            ->add(self::MEMBERS, DocumentType::class, [
                'class' => User::class,
                'choice_label' => 'username',
                'label' => 'label.members',
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
                'query_builder' => function (UserRepository $userRepository) {
                    $qb = $userRepository->createQueryBuilder();
                    $qb->field('deletedAt')->equals(null);
                    $qb->sort('username', RepositoryInterface::SORT_ASC);

                    return $qb;
                },
                'multiple' => true,
                'required' => false,
            ])
            ->add(self::MANAGERS, DocumentType::class, [
                'class' => User::class,
                'choice_label' => 'username',
                'label' => 'label.managers',
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
                'query_builder' => function (UserRepository $userRepository) {
                    $qb = $userRepository->createQueryBuilder();
                    $qb->field('deletedAt')->equals(null);
                    $qb->sort('username', RepositoryInterface::SORT_ASC);

                    return $qb;
                },
                'multiple' => true,
                'required' => false,
            ]);
    }
}
