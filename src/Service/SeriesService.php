<?php
declare(strict_types=1);

namespace App\Service;

use App\Controller\Query\SeriesFiltersQuery;
use App\Exceptions\Services\File\FileNotFoundException;
use App\Exceptions\Services\File\RealPathRequiredException;
use App\Model\Criteria\SeriesCriteria;
use App\Model\Interfaces\Model\BaseModelInterface;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Repository\SeriesRepositoryInterface;
use App\Model\Persistence\Series;
use App\Utils\Mapper\MapperInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Pagerfanta\Pagerfanta;
use ReflectionException;

/**
 * Class SeriesService
 * @package App\Service
 */
class SeriesService extends BaseModelService
{
    protected FileService $fileService;
    protected MapperInterface $mapper;

    /**
     * SeriesService constructor.
     *
     * @param SeriesRepositoryInterface $seriesRepository
     * @param FileService $fileService
     * @param MapperInterface $mapper
     */
    public function __construct(
        SeriesRepositoryInterface $seriesRepository,
        FileService $fileService,
        MapperInterface $mapper
    ) {
        $this->repository = $seriesRepository;
        $this->om = $seriesRepository->getObjectManager();
        $this->fileService = $fileService;
        $this->mapper = $mapper;
    }

    /**
     * @param BaseModelInterface $object
     *
     * @throws FileNotFoundException
     * @throws RealPathRequiredException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(BaseModelInterface $object): void
    {
        if ($object->getImage() instanceof ImageInterface && !is_null($object->getImage()->getRealPath())) {
            $file = $this->fileService->save($object->getImage());
            $object->setImage($file);
        }

        parent::save($object);
    }

    /**
     * @param string $id
     *
     * @return SeriesInterface|null
     */
    public function find(string $id): SeriesInterface|null
    {
        /** @var SeriesInterface $series */
        return $this->repository->find($id);
    }

    /**
     * @param array $criteria
     *
     * @return Series
     */
    public function findOneBy(array $criteria): Series
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param SeriesFiltersQuery $criteria
     *
     * @return mixed
     * @throws ReflectionException
     */
    public function findByCriteria(SeriesFiltersQuery $criteria): mixed
    {
        $criteria = $this->mapper->mapToObject($criteria, new SeriesCriteria());

        return $this->repository->findByCriteria($criteria);
    }

    /**
     * @param SeriesFiltersQuery $criteria
     * @param int $page
     *
     * @return Pagerfanta
     * @throws ReflectionException
     */
    public function findByCriteriaPaginated(SeriesFiltersQuery $criteria, int $page = 1): Pagerfanta
    {
        $criteria = $this->mapper->mapToObject($criteria, new SeriesCriteria());
        return $this->repository->findByCriteriaPaginated($criteria, $page);
    }
}
