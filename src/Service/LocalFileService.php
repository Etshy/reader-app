<?php
declare(strict_types=1);


namespace App\Service;

use App\Model\Interfaces\Model\Files\FileInterface;
use App\Model\Interfaces\Model\Files\MetadataInterface;
use App\Model\Interfaces\Repository\LocalFileRepositoryInterface;
use App\Model\Persistence\Files\LocalFile;
use App\Model\Persistence\Files\LocalImage;
use App\Utils\FileManager;
use DateTime;
use Exception;
use RuntimeException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LocalFileService
 * @package App\Service
 */
class LocalFileService extends BaseModelService
{
    /**
     * @var FileManager
     */
    protected FileManager $fileManager;


    /**
     * LocalFileService constructor.
     *
     * @param LocalFileRepositoryInterface $localFileRepository
     * @param FileManager $fileManager
     */
    public function __construct(LocalFileRepositoryInterface $localFileRepository, FileManager $fileManager)
    {
        $this->repository = $localFileRepository;
        $this->om = $localFileRepository->getObjectManager();
        $this->fileManager = $fileManager;
    }


    /**
     * @param array $criteria
     *
     * @return LocalFile
     */
    public function findOneBy(array $criteria): LocalFile
    {
        return $this->repository->findOneBy($criteria);
    }


    /**
     * @param string $id
     *
     * @return LocalFile
     */
    public function find(string $id): LocalFile
    {
        return $this->repository->find($id);
    }


    /**
     * @param UploadedFile $uploadedFile
     *
     * @param string $path
     *
     * @return LocalFile|LocalImage
     */
    public function createFileFromUploadedFile(UploadedFile $uploadedFile, string $path): LocalFile|LocalImage
    {
        $mime = $uploadedFile->getMimeType();
        if (str_contains($mime, 'image')) {
            $file = new LocalImage();
        } else {
            $file = new LocalFile();
        }
        $metadata = $this->fileManager->generateMetadataFromUploadedFile($uploadedFile);
        $file->setMetadata($metadata);
        $file->setName($uploadedFile->getClientOriginalName());
        $this->moveFile($uploadedFile, $path);
        $file->setRealPath($path.'/'.$uploadedFile->getClientOriginalName());
        $file->setCreatedAt(new DateTime());

        return $file;
    }

    /**
     * @param UploadedFile $file
     * @param string $path
     */
    public function moveFile(UploadedFile $file, string $path): void
    {
        $file->move($path, $file->getClientOriginalName());
    }

    /**
     * @param string $base64
     * @param string $filename
     * @param string $path
     *
     * @return LocalFile|LocalImage|null
     * @throws Exception
     */
    public function createFileFromBase64(string $base64, string $filename, string $path): LocalFile|LocalImage|null
    {
        if (!file_exists($path) && !mkdir($path, 0755, true) && !is_dir($path)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $path));
        }
        $ifp = fopen($path.'/'.$filename, 'wb');
        fwrite($ifp, base64_decode($base64));
        fclose($ifp);
        $mime = mime_content_type($path.'/'.$filename);
        if (str_contains($mime, 'image')) {
            $file = new LocalImage();
        } else {
            $file = new LocalFile();
        }
        $metadata = $this->fileManager->generateMetadataFromPathFile($path.'/'.$filename);
        if (!$metadata instanceof MetadataInterface) {
            throw new Exception();
        }
        $file->setMetadata($metadata);
        $file->setRealPath($path.'/'.$filename);
        $file->setName($filename);

        return $file;
    }

    /**
     * @param FileInterface $file
     */
    public function removeFile(FileInterface $file): void
    {
        $this->repository->getObjectManager()->remove($file);
        $this->repository->getObjectManager()->flush();
        $this->removeFileFromFileSystem($file);
    }

    /**
     * @param FileInterface $file
     */
    public function removeFileFromFileSystem(FileInterface $file): void
    {
        $realPath = $file->getRealPath();
        if (file_exists($realPath)) {
            unlink($realPath);
        }
    }
}
