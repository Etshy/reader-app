<?php
declare(strict_types=1);

namespace App\Service;

use App\Exceptions\Services\File\FileNotFoundException;
use App\Exceptions\Services\File\RealPathRequiredException;
use App\Form\Frontend\ReaderSettingsForm;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Interfaces\Repository\UserRepositoryInterface;
use App\Model\Persistence\User;
use App\Utils\CanonicalizerInterface;
use Doctrine\ORM\ORMException;
use Exception;
use Pagerfanta\Pagerfanta;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserService
 * @package App\Service
 */
class UserService extends BaseModelService
{
    protected TranslatorInterface $translator;
    protected EventDispatcherInterface $eventDispatcher;
    protected FileService $fileService;
    private UserPasswordHasherInterface $userPasswordEncoder;
    private CanonicalizerInterface $canonicalFieldsUpdater;


    /**
     * UserService constructor.
     *
     * @param UserRepositoryInterface $userRepository
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param CanonicalizerInterface $canonicalFieldsUpdater
     * @param TranslatorInterface $translator
     * @param EventDispatcherInterface $eventDispatcher
     * @param FileService $fileService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        UserPasswordHasherInterface $userPasswordHasher,
        CanonicalizerInterface $canonicalFieldsUpdater,
        TranslatorInterface $translator,
        EventDispatcherInterface $eventDispatcher,
        FileService $fileService
    ) {
        $this->repository = $userRepository;
        $this->om = $userRepository->getObjectManager();
        $this->userPasswordEncoder = $userPasswordHasher;
        $this->canonicalFieldsUpdater = $canonicalFieldsUpdater;
        $this->translator = $translator;
        $this->eventDispatcher = $eventDispatcher;
        $this->fileService = $fileService;
    }

    /**
     * @return UserInterface
     */
    public function createUser(): UserInterface
    {
        $object = $this->repository->getClassName();

        return new $object();
    }

    /**
     * @param UserInterface $user
     */
    public function deleteUser(UserInterface $user): void
    {
        $this->om->remove($user);
        $this->om->flush();
    }

    /**
     * @param string $id
     *
     * @return UserInterface|null
     */
    public function find(string $id): ?UserInterface
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $criteria
     *
     * @return UserInterface
     */
    public function findOneBy(array $criteria): UserInterface
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param string $email
     *
     * @return UserInterface|null
     */
    public function findUserByEmail(string $email): ?UserInterface
    {
        return $this->findUserBy(['emailCanonical' => $this->canonicalFieldsUpdater->canonicalize($email)]);
    }

    /**
     * @param array $criteria
     *
     * @return UserInterface|null
     */
    public function findUserBy(array $criteria): ?UserInterface
    {
        return $this->getRepository()->findOneBy($criteria);
    }

    /**
     * @param string $username
     *
     * @return UserInterface|null
     */
    public function findUserByUsername(string $username): ?UserInterface
    {
        return $this->findUserBy(['usernameCanonical' => $this->canonicalFieldsUpdater->canonicalize($username)]);
    }

    /**
     * @param string $token
     *
     * @return UserInterface|null
     */
    public function findUserByConfirmationToken(string $token): ?UserInterface
    {
        return $this->findUserBy(['confirmationToken' => $token]);
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return User::class;
    }

    /**
     * @param UserInterface $user
     *
     * @throws ORMException
     */
    public function reloadUser(UserInterface $user): void
    {
        $this->getOm()->refresh($user);
    }

    /**
     * @throws RealPathRequiredException
     * @throws FileNotFoundException
     */
    public function updateUser(UserInterface $user, bool $andFlush = true): void
    {
        $this->updateCanonicalFields($user);

        if ($user->getAvatar() instanceof ImageInterface
            && !is_null($user->getAvatar()->getRealPath())) {
            $avatarProxy = $this->fileService->save($user->getAvatar());
            $user->setAvatar($avatarProxy);
        }
        $this->om->persist($user);
        if ($andFlush) {
            $this->om->flush();
        }
    }

    /**
     * @param UserInterface $user
     */
    public function updateCanonicalFields(UserInterface $user): void
    {
        $user->setUsernameCanonical($this->canonicalFieldsUpdater->canonicalize($user->getUsername()));
        $user->setEmailCanonical($this->canonicalFieldsUpdater->canonicalize($user->getEmail()));
    }

    /**
     * @param UserInterface $user
     * @param string $password
     */
    public function updatePassword(UserInterface $user, string $password): void
    {
        $password = $this->userPasswordEncoder->hashPassword($user, $password);
        $user->setPassword($password);
    }

    /**
     * @param array $criteria
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function findByCriteriaPaginated(array $criteria = [], int $page = 1): Pagerfanta
    {
        return $this->repository->findByCriteriaPaginated($criteria, $page);
    }

    /**
     * @param UserInterface $user
     * @param array $data
     *
     * @throws Exception
     */
    public function updateReaderPreferences(UserInterface $user, array $data): void
    {
        $user->getSettings()?->setReaderMode($data[ReaderSettingsForm::READER_MODE]);
        $user->getSettings()?->setReaderDirection($data[ReaderSettingsForm::READER_DIRECTION]);
        $this->updateUser($user);
    }

}
