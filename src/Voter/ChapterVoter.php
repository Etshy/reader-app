<?php
declare(strict_types=1);

namespace App\Voter;

use App\Model\Interfaces\Model\ChapterInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Persistence\Chapter;
use App\Model\Persistence\Team;
use App\Model\Persistence\User;
use App\Utils\Traits\ConstantsTrait;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class ChapterVoter
 * @package App\Voter
 */
class ChapterVoter extends Voter
{
    use ConstantsTrait;

    //Chapters Roles
    public const ROLE_LIST_CHAPTER = 'ROLE_LIST_CHAPTER';
    public const ROLE_ADD_CHAPTER = 'ROLE_ADD_CHAPTER';
    public const ROLE_EDIT_CHAPTER = 'ROLE_EDIT_CHAPTER';
    public const ROLE_VIEW_CHAPTER = 'ROLE_VIEW_CHAPTER';
    public const ROLE_DELETE_CHAPTER = 'ROLE_DELETE_CHAPTER';

    private AccessDecisionManagerInterface $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports(string $attribute, $subject): bool
    {
        //$constants = $this->getConstants();
        $constants = [
            self::ROLE_LIST_CHAPTER,
            self::ROLE_ADD_CHAPTER,
            self::ROLE_EDIT_CHAPTER,
            self::ROLE_VIEW_CHAPTER,
            self::ROLE_DELETE_CHAPTER,
        ];

        // if the attribute isn't one we support, return false
        if (!in_array($attribute, $constants)) {
            return false;
        }

        //For add and list perms, we don't have a series to control, so we bypass that.
        if (!in_array($attribute, [self::ROLE_ADD_CHAPTER, self::ROLE_LIST_CHAPTER]) && !$subject instanceof ChapterInterface) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        //ADMIN and SUPER_ADMIN can do anything they want !
        if ($this->decisionManager->decide($token, [UserVoter::ROLE_ADMIN])) {
            return true;
        }

        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            // the user must be logged in. if not, deny access
            return false;
        }

        $chapter = $subject;
        switch ($attribute) {
            case self::ROLE_DELETE_CHAPTER:
            case self::ROLE_ADD_CHAPTER:
            case self::ROLE_LIST_CHAPTER:
                if (in_array($attribute, $user->getRoles())) {
                    return true;
                }
                break;

            case self::ROLE_VIEW_CHAPTER:
                if (in_array($attribute, $user->getRoles())) {
                    return $this->canView($chapter, $user);
                }
                break;

            case self::ROLE_EDIT_CHAPTER:
                if (in_array($attribute, $user->getRoles())) {
                    return $this->canEdit($chapter, $user);
                }
                break;

            default:
                throw new LogicException('This code should not be reached!');
        }

        return false;
    }

    private function canView(ChapterInterface $chapter, UserInterface $user): bool
    {
        if ($chapter instanceof Chapter) {
            //if user can edit, he can view.
            return $this->canEdit($chapter, $user);
        }

        return false;
    }

    private function canEdit(ChapterInterface $chapter, UserInterface $user): bool
    {
        if ($chapter instanceof Chapter) {
            //if user can edit, he can view.
            return $this->userIsManagerOfChapter($chapter, $user);
        }

        return false;
    }

    private function userIsManagerOfChapter(ChapterInterface $chapter, UserInterface $user): bool
    {
        $teams = $chapter->getTeams();

        //We search in the Teams related to the Series
        foreach ($teams as $team) {
            if (!$team instanceof Team) {
                continue;
            }

            //We search in the Managers of the team
            $members = $team->getManagers();

            foreach ($members as $member) {
                if (!$member instanceof User) {
                    continue;
                }
                //if the current User is in the managers, he can update it
                if ($member->getId() === $user->getId()) {
                    return true;
                }
            }
        }

        return false;
    }
}
