<?php
declare(strict_types=1);

namespace App\Controller\Frontend;

use App\Controller\Query\SeriesFiltersQuery;
use App\Exceptions\Controller\SeriesNotFoundException;
use App\Form\Frontend\SeriesFiltersForm;
use App\Model\Interfaces\Model\FollowInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Service\ChapterReadenService;
use App\Service\FollowService;
use App\Service\SeriesService;
use App\Utils\Context\ContextAccessor;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use MongoDuplicateKeyException;
use ReflectionException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class SeriesController
 * @package App\Controller\Frontend
 */
#[Route('/series', name: 'public_series_')]
class SeriesController extends AbstractController
{

    protected SeriesService $seriesService;
    protected FollowService $followService;
    protected TranslatorInterface $translator;
    protected ContextAccessor $contextAccessor;
    private ChapterReadenService $chapterReadenService;

    public function __construct(
        SeriesService $seriesService,
        FollowService $followService,
        TranslatorInterface $translator,
        ChapterReadenService $chapterReadenService,
        ContextAccessor $contextAccessor,
    ) {
        $this->seriesService = $seriesService;
        $this->followService = $followService;
        $this->translator = $translator;
        $this->chapterReadenService = $chapterReadenService;
        $this->contextAccessor = $contextAccessor;
    }

    /**
     * @throws ReflectionException
     */
    #[Route('/{page}', name: 'list', requirements: ['page' => '\d*'], defaults: ['page' => 1])]
    #[Route('/a/{authorOrArtist}/{page}', name: 'list_a', requirements: ['authorOrArtist' => '[\w :-_]*', 'page' => '\d*'], defaults: ['page' => 1])]
    #[Route('/tag/{tags}/{page}', name: 'list_tags', requirements: ['tags' => '\w*', 'page' => '\d*'], defaults: ['page' => 1])]
    public function list(int $page, SeriesFiltersQuery $seriesFiltersQuery, string $tags = null): Response
    {
        $seriesFiltersQuery->setVisible(true);
        if ($tags) {
            $seriesFiltersQuery->setIncludeTags([$tags]);
        }
        $pagination = $this->seriesService->findByCriteriaPaginated($seriesFiltersQuery, $page);

        $form = $this->createForm(SeriesFiltersForm::class, $seriesFiltersQuery);

        return $this->render('frontend/series/list.html.twig', [
            'pagination' => $pagination,
            'settings' => $this->contextAccessor->getContext()->getSettings(),
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{slug}', name: 'details', requirements: ['slug' => '[a-zA-Z0-9_-]+'])]
    public function details(string $slug): Response
    {
        $series = $this->seriesService->findOneBy([
            'slug' => $slug,
        ]);

        if (!$series instanceof SeriesInterface) {
            throw $this->createNotFoundException($this->translator->trans('error.not_found.series'));
        }

        $follow = null;
        if ($this->getUser() instanceof UserInterface) {
            $follow = $this->followService->findOneBy([
                'user.id' => $this->getUser()->getId(),
                'series.id' => $series->getId(),
            ]);
        }

        $seriesFollowed = false;
        if ($follow instanceof FollowInterface) {
            $seriesFollowed = true;
        }

        $idsChaptersRead = $this->chapterReadenService->getArrayChapterIdsBySeries($series);

        return $this->render('frontend/series/details.html.twig', [
            'series' => $series,
            'seriesFollowed' => $seriesFollowed,
            'idsChaptersRead' => $idsChaptersRead,
            'settings' => $this->contextAccessor->getContext()->getSettings(),
        ]);
    }

    /**
     * @throws ORMException
     * @throws SeriesNotFoundException
     */
    #[Route('/follow/{id}', name: 'follow', requirements: ['id' => '\w+'])]
    #[IsGranted('ROLE_USER')]
    public function follow($id): RedirectResponse
    {
        $series = $this->seriesService->find($id);

        if (!$series instanceof SeriesInterface) {
            throw new SeriesNotFoundException();
        }

        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            throw new UserNotFoundException();
        }

        try {
            $this->followService->followSeries($user, $series);
        } catch (MongoDuplicateKeyException) {
            $this->addFlash(
                'success',
                $this->translator->trans('error.series.already-followed')
            );

            return $this->redirectToRoute('public_series_details', ['slug' => $series->getSlug()]);
        }
        $this->addFlash(
            'success',
            $this->translator->trans('series.followed')
        );

        return $this->redirectToRoute('public_series_details', ['slug' => $series->getSlug()]);
    }

    /**
     * @param $id
     *
     * @return RedirectResponse
     * @throws MongoDBException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws SeriesNotFoundException
     */
    #[Route('/unfollow/{id}', name: 'unfollow', requirements: ['id' => '\w+'])]
    #[IsGranted("ROLE_USER")]
    public function unfollow($id): RedirectResponse
    {
        $series = $this->seriesService->find($id);

        if (!$series instanceof SeriesInterface) {
            throw new SeriesNotFoundException();
        }

        /** @var UserInterface $user */
        $user = $this->getUser();
        $this->followService->unfollowSeries($user, $series);
        $this->addFlash(
            'success',
            $this->translator->trans('series.unfollowed')
        );

        return $this->redirectToRoute('public_series_details', ['slug' => $series->getSlug()]);
    }
}
