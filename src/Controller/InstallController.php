<?php declare(strict_types=1);

namespace App\Controller;

use App\Form\InstallForm;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InstallController extends AbstractController
{
    protected string $rootPath;

    public function __construct(string $rootPath)
    {
        $this->rootPath = $rootPath;
    }

    /**
     * @throws Exception
     */
    #[Route('/install', name: 'app_install')]
    public function install(Request $request): RedirectResponse|Response
    {
        if (array_key_exists('INSTALLED', $_ENV)) {
            return $this->redirect($this->generateUrl('homepage'));
        }

        $form = $this->createForm(InstallForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $envFileData = file($this->rootPath.'/.env');
                $envData = [];

                foreach ($envFileData as $row) {
                    if ($row === PHP_EOL || str_starts_with($row, '#')) {
                        continue;
                    }
                    $dataRow = explode('=', $row);
                    $key = array_shift($dataRow);
                    $envData[trim($key)] = trim(implode('=', $dataRow));
                }
                $formData = $form->getData();

                $file = fopen($this->rootPath.'/.env', 'wb');
                fwrite($file, 'VERSION='.$envData['VERSION'].PHP_EOL);//keep the verison
                fwrite($file, 'APP_ENV='.$envData['APP_ENV'].PHP_EOL);//keep the env
                fwrite($file, 'APP_SECRET='.bin2hex(random_bytes(16)).PHP_EOL); // generate new secret
                fwrite($file, 'MESSENGER_ASYNC_TRANSPORT_DSN='.$envData['MESSENGER_ASYNC_TRANSPORT_DSN'].PHP_EOL); // keep async uri
                fwrite($file, 'DB_DRIVER='.$formData[InstallForm::DATABASE_TYPE].PHP_EOL); // Add db driver to the one chosed
                switch ($formData[InstallForm::DATABASE_TYPE]) {
                    // Add DB connection string
                    case InstallForm::DATABASE_MONGO:
                        fwrite($file, 'MONGODB_URL='.$formData[InstallForm::DATABASE_CONNECTION_STRING].PHP_EOL);
                        fwrite($file, 'MONGODB_DB='.$formData[InstallForm::DATABASE_NAME].PHP_EOL);
                        break;
                    case InstallForm::DATABASE_ORM:
                        fwrite($file, 'DATABASE_URL='.$formData[InstallForm::DATABASE_CONNECTION_STRING].'/'.$formData[InstallForm::DATABASE_NAME].PHP_EOL);
                        break;
                    default:
                        throw new Exception('Invalid Database type');
                }
                fwrite($file, 'MAILER_DSN='.$formData[InstallForm::MAILER_DSN].PHP_EOL);
                fwrite($file, 'EMAIL_SENDER_ADDRESS='.$formData[InstallForm::MAIL_SENDER_ADDRESS].PHP_EOL);
                fwrite($file, 'EMAIL_SENDER_NAME='.$formData[InstallForm::MAIL_SENDER_NAME].PHP_EOL);
                fwrite($file, 'DEFAULT_LOCALE='.$formData[InstallForm::DEFAULT_LOCALE].PHP_EOL);
                fwrite($file, 'INSTALLED=true'.PHP_EOL);
                fclose($file);

                return new RedirectResponse($this->generateUrl('homepage'));
            }
        }

        return $this->render('install/install.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
