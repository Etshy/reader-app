<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Interfaces\Model\NotificationInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Service\NotificationService;
use Exception;
use Hhxsv5\SSE\Event;
use Hhxsv5\SSE\SSE;
use Hhxsv5\SSE\StopSSEException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

/**
 * Class NotificationController
 * Controller to use for SSE and WebSocket
 * @package App\Controller
 */
class NotificationController extends AbstractController
{
    protected TranslatorInterface $translator;
    private NotificationService $notificationService;

    public function __construct(NotificationService $notificationService, TranslatorInterface $translator)
    {
        $this->notificationService = $notificationService;
        $this->translator = $translator;
    }

    /**
     * @return JsonResponse
     */
    #[Route("/notifications/latests", name: "notification_latests")]
    #[Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")]
    public function latests(): JsonResponse
    {
        /** @var UserInterface $user */
        $user = $this->getUser();

        $jsonResponse = new JsonResponse();

        $count = $this->notificationService->countUnread($user);

        $notifications = $this->notificationService->findLatestNotifications($user);

        $retour = [
            'notifications' => array_values($notifications->toArray()),
            'unreadCount' => $count,
        ];

        $this->notificationService->setAllAsSentForUser($user);

        $jsonResponse->setData($retour);

        return $jsonResponse;
    }

    /**
     * @throws Exception
     */
    #[Route('/notifications/read/{id}', name: 'notifications_read')]
    #[Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")]
    public function read($id): JsonResponse
    {
        $notification = $this->notificationService->find($id);

        if (!$notification instanceof NotificationInterface) {
            throw $this->createNotFoundException($this->translator->trans('error.not_found.notification'));
        }

        try {
            $this->notificationService->setAsReaden($notification);
        } catch (Exception) {
            throw new Exception($this->translator->trans('error.occurred'));
        }

        $json = new JsonResponse();
        $json->setData([
            'success' => true,
        ]);

        return $json;
    }

    #[Route("/notifications/sse", name: "notification_sse")]
    #[Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")]
    public function notificationSSE(): StreamedResponse
    {
        /** @var UserInterface $user */
        $user = $this->getUser();
        
        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('Cache-Control', 'no-cache');
        $response->headers->set('Connection', 'keep-alive');
        $response->headers->set('X-Accel-Buffering', 'no');
        
        $response->setCallback(function () use ($user) {
            $callback = function () use ($user) {
                try {
                    $notifications = $this->notificationService->getUnreadUnsentNotificationsForUser($user)->toArray();
                    $notifications = array_values($notifications);
                    if (empty($notifications)) {
                        return json_encode([
                            'error' => 'no-data',
                        ], JSON_THROW_ON_ERROR); // Return false if no new notifications
                    }
                    /*$shouldStop = false;// Stop if something happens or to clear connection, browser will retry
                    if ($shouldStop) {
                        throw new StopSSEException();
                    }*/
                    $return = [];
                    foreach ($notifications as $notification) {
                        $return[] = $notification->toArray();
                    }
                    $return = json_encode([
                        "data" => $return,
                    ], JSON_THROW_ON_ERROR);
                    $this->notificationService->setAllAsSentForUser($user);

                    return $return;
                } catch (Throwable) {
                    throw new StopSSEException();
                }
            };
            (new SSE(new Event($callback, 'notifications', 60000)))->start(60);
        });

        return $response;
    }
}
