<?php
declare(strict_types=1);

namespace App\Controller\Backend;

use App\Exceptions\Controller\ChapterNotFoundException;
use App\Form\Backend\Chapter\ChapterForm;
use App\Model\Interfaces\Model\ChapterInterface;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Model\PageInterface;
use App\Model\Persistence\Chapter;
use App\Model\Persistence\Embed\Page;
use App\Service\ChapterService;
use App\Service\LocalFileService;
use App\Service\SeriesService;
use App\Utils\Context\ContextAccessor;
use ArrayIterator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ChapterController
 * @package App\Controller\Backend
 */
#[Route("/admin/chapter", name: "admin_chapter_")]
class ChapterController extends AbstractController
{
    protected EventDispatcherInterface $dispatcher;
    protected ContextAccessor $contextAccessor;
    private SeriesService $seriesService;
    private ChapterService $chapterService;
    private LocalFileService $fileService;
    private TranslatorInterface $translator;

    public function __construct(
        SeriesService $seriesService,
        ChapterService $chapterService,
        TranslatorInterface $translator,
        LocalFileService $fileService,
        EventDispatcherInterface $dispatcher,
        ContextAccessor $contextAccessor,
    ) {
        $this->seriesService = $seriesService;
        $this->chapterService = $chapterService;
        $this->translator = $translator;
        $this->fileService = $fileService;
        $this->dispatcher = $dispatcher;
        $this->contextAccessor = $contextAccessor;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    #[Route("/add/{seriesId}", name: 'add', defaults: ["seriesId" => null])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_ADD_CHAPTER')")]
    public function add(
        $seriesId,
        Request $request
    ): RedirectResponse|Response {
        $chapter = new Chapter();

        if ($seriesId) {
            $series = $this->seriesService->find($seriesId);
            $chapter->setSeries($series);
        }

        $form = $this->createForm(ChapterForm::class, $chapter, [
            'seriesId' => $seriesId,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->chapterService->isChapterAlreadyExists($chapter)) {
                $this->addFlash(
                    'error',
                    $this->translator->trans('chapter.already.exists')
                );

                return $this->redirectToRoute(
                    'admin_chapter_add',
                    [
                        'seriesId' => $seriesId,
                    ]
                );
            }

            $this->chapterService->save($chapter);
            $this->addFlash(
                'success',
                $this->translator->trans('chapter.succesfully.created')
            );

            return $this->redirectToRoute('admin_chapter_edit', [
                'id' => $chapter->getId(),
            ]);
        }

        return $this->render('backend/chapter/add.html.twig', [
            'form' => $form->createView(),
            'chapter' => $chapter,
        ]);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ChapterNotFoundException
     */
    #[Route('/edit/{id}', name: 'edit', requirements: ['id' => '\w+'])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_CHAPTER')")]
    public function edit(
        string $id,
        Request $request
    ): RedirectResponse|Response {
        $chapter = $this->chapterService->find($id);

        if (!$chapter instanceof ChapterInterface) {
            throw new ChapterNotFoundException();
        }

        $form = $this->createForm(ChapterForm::class, $chapter);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->chapterService->save($chapter);
            $this->addFlash(
                'success',
                $this->translator->trans('chapter.succesfully.edited')
            );

            return $this->redirectToRoute('admin_chapter_edit', [
                'id' => $chapter->getId(),
            ]);
        }

        return $this->render('backend/chapter/add.html.twig', [
            'form' => $form->createView(),
            'chapter' => $chapter,
            'settings' => $this->contextAccessor->getContext()->getSettings(),
        ]);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    #[Route('/upload-page', name: 'upload_page')]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_CHAPTER')")]
    public function uploadPage(
        Request $request
    ): JsonResponse {
        /** @var ArrayIterator|array $post */
        $post = $request->request->getIterator()->getArrayCopy();

        $scrambledImage = null;
        if ($request->files->has('imageScrambled') && $request->files->count() === 2) {
            $file = $request->files->get('file');
            $scrambledImage = $request->files->get('imageScrambled');
        } elseif ($request->files->count() === 1) {
            /** @var UploadedFile $file */
            $file = $request->files->getIterator()->current();
        } else {
            return new JsonResponse($this->translator->trans('upload.error.one_image_max'), 400);
        }

        $chapter = $this->chapterService->find($post['chapterId']);

        if (!$chapter instanceof ChapterInterface) {
            throw new ChapterNotFoundException();
        }

        //TODO Move that ion a PageService or something like that
        $page = new Page();
        $page->setOrder($request->request->getInt('order'));
        $path = $this->chapterService->getChapterImageFolder($chapter);
        $image = $this->fileService->createFileFromUploadedFile($file, $path);
        $this->fileService->save($image);
        $page->setImage($image);
        $settings = $this->contextAccessor->getContext()->getSettings();
        if (!is_null($scrambledImage)
            && $settings->isImageScrambled()
        ) {
            $path .= '/scrambled';
            $image = $this->fileService->createFileFromUploadedFile($scrambledImage, $path);
            $this->fileService->save($image);
            $page->setScrambledImage($image);
        }
        $chapter->addPage($page);
        $chapter = $this->chapterService->sortPages($chapter);

        $this->chapterService->save($chapter);

        $deleteUrl = $this->generateUrl('admin_chapter_delete_page', [
            'id' => $chapter->getId(),
            'order' => $page->getOrder(),
        ]);
        $fileUrl = $this->generateUrl('render_image', [
            'id' => $image->getId(),
        ]);

        $return = [
            'delete_method' => 'DELETE',
            'delete_url' => $deleteUrl,
            'file_url' => $fileUrl,
        ];
        $jsonResponse = new JsonResponse();
        $jsonResponse->setData($return);

        return $jsonResponse;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ChapterNotFoundException
     */
    #[Route('/delete-page/{id}/{order}', name: 'delete_page', requirements: ['order' => '\d+', 'id' => '\w+'])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_CHAPTER')")]
    public function deletePage(
        string $id,
        int $order
    ): JsonResponse {
        $chapter = $this->chapterService->find($id);

        if (!$chapter instanceof ChapterInterface) {
            throw new ChapterNotFoundException();
        }

        $page = $this->chapterService->getPage($chapter, $order);

        if (!$page instanceof PageInterface) {
            return new JsonResponse($this->translator->trans('page.not_found'), Response::HTTP_NOT_FOUND);
        }

        //remove page from chapter
        $chapter->removePage($page);
        $this->chapterService->save($chapter);
        //remove image from filesystem
        $this->fileService->removeFile($page->getImage());
        if ($page->getScrambledImage() instanceof ImageInterface) {
            $this->fileService->removeFile($page->getScrambledImage());
        }

        $jsonResponse = new JsonResponse();
        $return = [
            'success' => true,
        ];
        $jsonResponse->setData($return);

        return $jsonResponse;
    }

    /**
     * @throws ChapterNotFoundException
     */
    #[Route('/delete-all-pages/{id}', name: 'delete_all_pages', requirements: ['id' => '\w+'], methods: 'DELETE')]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_CHAPTER')")]
    public function deleteAllPages(
        string $id
    ): JsonResponse {
        $jsonResponse = new JsonResponse();

        $chapter = $this->chapterService->find($id);

        if (!$chapter instanceof ChapterInterface) {
            throw new ChapterNotFoundException();
        }

        $pages = $chapter->getPages();
        $imagesId = [];
        foreach ($pages as $page) {
            if (!$page instanceof PageInterface) {
                continue;
            }
            $imagesId[] = $page->getImage()->getId();

            //remove image from filesystem
            $this->fileService->removeFileFromFileSystem($page->getImage());
            if ($page->getScrambledImage() instanceof ImageInterface) {
                $this->fileService->removeFileFromFileSystem($page->getScrambledImage());
            }
        }
        try {
            $this->fileService->batchRemove($imagesId);
        } catch (Exception) {
            $return = [
                'success' => false,
                'message' => $this->translator->trans('error.occurred'),
            ];
            $jsonResponse->setData($return);

            return $jsonResponse;
        }

        $chapter->setPages(new ArrayCollection());

        try {
            $this->chapterService->save($chapter);
        } catch (Exception) {
            $return = [
                'success' => false,
                'message' => $this->translator->trans('error.occurred.deleting_chapter_pages'),
            ];
            $jsonResponse->setData($return);

            return $jsonResponse;
        }

        $return = [
            'success' => true,
        ];
        $jsonResponse->setData($return);

        return $jsonResponse;
    }

    /**
     * @throws ChapterNotFoundException
     */
    #[Route('/update-page-order/{id}', name: 'update_page_order', requirements: ['id' => '\w+'])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_CHAPTER')")]
    public function updatePageOrder(
        Request $request,
        string $id
    ): JsonResponse {
        $post = $request->request->getIterator();
        $post = $post->getArrayCopy();
        $chapter = $this->chapterService->find($id);

        if (!$chapter instanceof ChapterInterface) {
            throw new ChapterNotFoundException();
        }

        $jsonResponse = new JsonResponse();
        if (!array_key_exists('data', $post)) {
            $return = [
                'success' => false,
                'message' => $this->translator->trans('error.missing_parameters.data'),
            ];
            $jsonResponse->setData($return);

            return $jsonResponse;
        }

        $data = $post['data'];
        $chapter = $this->chapterService->sortPagesByOrdersArray($chapter, $data);
        try {
            $this->chapterService->save($chapter);
        } catch (Exception) {
        }

        $return = [
            'success' => true,
        ];
        $jsonResponse->setData($return);

        return $jsonResponse;
    }

    /**
     * @throws ORMException
     */
    #[Route('/delete/{id}', name: 'delete', requirements: ['id' => '\w+'], methods: 'DELETE')]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DELETE_CHAPTER')")]
    public function delete(
        string $id
    ): JsonResponse {
        $jsonResponse = new JsonResponse();

        $chapter = $this->chapterService->find($id);

        if (!$chapter instanceof Chapter) {
            $return = [
                'success' => false,
                'message' => $this->translator->trans('error.not_found.chapter'),
            ];
            $jsonResponse->setData($return);
            $jsonResponse->setStatusCode(404);

            return $jsonResponse;
        }

        $this->chapterService->remove($chapter);

        $return = [
            'success' => true,
        ];
        $jsonResponse->setData($return);

        return $jsonResponse;
    }
}
