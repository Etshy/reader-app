<?php declare(strict_types=1);

namespace App\Controller\Backend;

use App\Exceptions\Services\File\FileNotFoundException;
use App\Exceptions\Services\File\RealPathRequiredException;
use App\Form\Backend\Users\UserForm;
use App\Model\Interfaces\Model\UserInterface;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UsersController
 * @package App\Controller\Backend
 */
#[Route('/admin/users', name: 'admin_users_')]
class UsersController extends AbstractController
{
    private UserService $userService;
    private TranslatorInterface $translator;

    public function __construct(UserService $userService, TranslatorInterface $translator)
    {
        $this->userService = $userService;
        $this->translator = $translator;
    }

    #[Route('/page/{page}', name: 'list', requirements: ['id' => '\d+'], defaults: ['page' => 1])]
    #[Route('/', name: 'list')]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LIST_USERS')")]
    public function list(int $page = 1): Response
    {
        $pagination = $this->userService->findByCriteriaPaginated([], $page);

        return $this->render(
            'backend/users/list.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }

    /**
     * @throws RealPathRequiredException
     * @throws FileNotFoundException
     */
    #[Route('/edit/{id}', name: 'edit', requirements: ['id' => '\w+'])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_USER')")]
    public function edit(string $id, Request $request): Response
    {
        $user = $this->userService->find($id);

        if (!$user instanceof UserInterface) {
            throw new UserNotFoundException();
        }

        $form = $this->createForm(UserForm::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->userService->updateUser($user);

            $this->addFlash(
                'success',
                $this->translator->trans('user.successfully.edited')
            );

            return $this->redirectToRoute(
                'admin_users_edit',
                [
                    'id' => $user->getId(),
                ]
            );
        }

        return $this->render(
            'backend/users/edit.html.twig',
            [
                'user' => $user,
                'form' => $form->createView(),
            ]
        );
    }
}
