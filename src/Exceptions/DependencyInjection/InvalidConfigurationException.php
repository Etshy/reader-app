<?php
declare(strict_types=1);

namespace App\Exceptions\DependencyInjection;

use Exception;

/**
 * Class InvalidConfigurationException
 * @package App\Exceptions\DependencyInjection
 */
class InvalidConfigurationException extends Exception
{
}
