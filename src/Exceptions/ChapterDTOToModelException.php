<?php
declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class ChapterDTOToModelException
 * @package App\Exceptions
 */
class ChapterDTOToModelException extends Exception
{
}
