<?php declare(strict_types=1);

namespace App\Exceptions\Services\File;

use Exception;

/**
 * Class FileNotFoundException
 * @package App\Exceptions\Services\File
 */
class FileNotFoundException extends Exception
{
}
