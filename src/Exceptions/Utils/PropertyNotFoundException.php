<?php declare(strict_types=1);

namespace App\Exceptions\Utils;

use Exception;

/**
 * Class PropertyNotFoundException
 * @package App\Exceptions\Utils
 */
class PropertyNotFoundException extends Exception
{
}
