<?php declare(strict_types=1);

namespace App\Event\Event;

use App\Model\Interfaces\Model\ChapterInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class ChapterPublished
 * @package App\Event\Event
 */
class ChapterPublished extends Event
{
    private ?ChapterInterface $chapter;

    public function __construct(?ChapterInterface $chapter=null)
    {
        $this->chapter = $chapter;
    }

    public function getChapter(): ?ChapterInterface
    {
        return $this->chapter;
    }

    public function setChapter(?ChapterInterface $chapter): ChapterPublished
    {
        $this->chapter = $chapter;

        return $this;
    }
}
