<?php
declare(strict_types=1);

namespace App\Utils;

/**
 * Interface CanonicalizerInterface
 * @package App\Utils
 */
interface CanonicalizerInterface
{
    public function canonicalize(string $string): ?string;
}
