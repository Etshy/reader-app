<?php declare(strict_types=1);

namespace App\Utils;

use App\Exceptions\Utils\PropertyNotFoundException;
use JetBrains\PhpStorm\Pure;
use ReflectionClass;
use ReflectionProperty;

/**
 * Class CustomReflection
 * @package App\Utils
 */
class CustomReflection extends ReflectionClass
{
    #[Pure]
    public function getPropertiesRecursive(): array
    {
        $propertiesToMerge = [];
        $properties = [];
        $rc = $this;
        do {
            $rp = [];
            foreach ($rc->getProperties() as $p) {
                $rp[$p->getName()] = $p;
            }
            $propertiesToMerge[] = $rp;
        } while ($rc = $rc->getParentClass());

        return array_merge($properties, ...$propertiesToMerge);
    }

    /**
     * @throws PropertyNotFoundException
     */
    public function getPropertyRecusive(string $property): ReflectionProperty
    {
        $rc = $this;
        do {
            foreach ($rc->getProperties() as $p) {
                if ($p->getName() === $property) {
                    return $p;
                }
            }
        } while ($rc = $rc->getParentClass());
        throw new PropertyNotFoundException();
    }

    #[Pure]
    public function hasPropertyRecursive(string $property): bool
    {
        $rc = $this;
        do {
            foreach ($rc->getProperties() as $p) {
                if ($p->getName() === $property) {
                    return true;
                }
            }
        } while ($rc = $rc->getParentClass());

        return false;
    }

}
