<?php
declare(strict_types=1);


namespace App\Utils;

use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Persistence\Files\FileMetadata;
use App\Model\Persistence\Files\ImageMetadata;
use claviska\SimpleImage;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FileManager
 * @package App\Utils
 */
class FileManager
{
    public function generateMetadataFromUploadedFile(UploadedFile $uploadedFile): ImageMetadata|FileMetadata
    {
        $mime = $uploadedFile->getMimeType();

        if (str_contains($mime, 'image')) {
            $path = $uploadedFile->getRealPath();
            [$width, $height] = getimagesize($path);
            $fileMetadata = new ImageMetadata();
            $fileMetadata->setMime($uploadedFile->getMimeType());
            $fileMetadata->setMd5(md5_file($uploadedFile->getRealPath()));
            $fileMetadata->setWidth($width);
            $fileMetadata->setHeight($height);
        } else {
            $fileMetadata = new FileMetadata();
            $fileMetadata->setMime($uploadedFile->getMimeType());
            $fileMetadata->setMd5(md5_file($uploadedFile->getRealPath()));
        }

        return $fileMetadata;
    }

    public function generateMetadataFromPathFile(string $pathFile): ImageMetadata|FileMetadata|null
    {
        if (!$pathFile || !file_exists($pathFile)) {
            return null;
        }

        $mime = mime_content_type($pathFile);

        if (str_contains($mime, 'image')) {
            [$width, $height] = getimagesize($pathFile);
            $fileMetadata = new ImageMetadata();
            $fileMetadata->setMime($mime);
            $fileMetadata->setMd5(md5_file($pathFile));
            $fileMetadata->setWidth($width);
            $fileMetadata->setHeight($height);
        } else {
            $fileMetadata = new FileMetadata();
            $fileMetadata->setMime($mime);
            $fileMetadata->setMd5(md5_file($pathFile));
        }

        return $fileMetadata;
    }

    /**
     * @throws Exception
     */
    public function createSimpleImageFromImage(ImageInterface $image): SimpleImage
    {
        $filename = $image->getRealPath();
        $simpleImage = new SimpleImage();
        $simpleImage->fromFile($filename);

        return $simpleImage;
    }

    public function resizeImage(SimpleImage $simpleImage, int $width = null, int $height = null, int $maxwidth = null, int $maxheight = null): SimpleImage
    {
        if (!is_null($maxwidth) && $simpleImage->getWidth() > $maxwidth) {
            $simpleImage->resize($maxwidth);

            return $simpleImage;
        }
        if (!is_null($maxheight) && $simpleImage->getHeight() > $maxheight) {
            $simpleImage->resize(null, $maxheight);

            return $simpleImage;
        }

        if (!is_null($width) || !is_null($height)) {
            $simpleImage->resize($width, $height);
        }
        return $simpleImage;
    }
}
