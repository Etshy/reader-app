<?php declare(strict_types=1);

namespace App\Utils\Context;

class ContextAccessor
{
    private Context $context;

    public function initContext(Context $context): void
    {
        $this->context = $context;
    }

    public function getContext(): Context
    {
        return $this->context;
    }
}
