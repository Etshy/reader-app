<?php declare(strict_types=1);

namespace App\Utils\Context;

use App\Model\Interfaces\Model\SettingsInterface;

class Context
{
    private SettingsInterface $settings;

    public function getSettings(): SettingsInterface
    {
        return $this->settings;
    }

    public function setSettings(SettingsInterface $settings): Context
    {
        $this->settings = $settings;

        return $this;
    }
}
