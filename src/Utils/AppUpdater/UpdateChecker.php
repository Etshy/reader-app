<?php declare(strict_types=1);

namespace App\Utils\AppUpdater;

use Gitlab\Client;

class UpdateChecker
{


    //TODO methods to check latests tags/release on gitlab
    protected Client $gitlabClient;

    public function __construct(Client $gitlabClient)
    {
        $this->gitlabClient = $gitlabClient;
    }

    public function isUpdateAvailable(string $lastVersion): bool
    {
        $lastVersionArray = explode('.', $lastVersion);
        $actualVersionArray = explode('.', $_ENV['VERSION']);

        foreach ($lastVersionArray as $key => $each_number) {
            $lastVersionArray[$key] = (int)$each_number;
        }
        foreach ($actualVersionArray as $key => $each_number) {
            $actualVersionArray[$key] = (int)$each_number;
        }

        if ($lastVersionArray[0] > $actualVersionArray[0]) {
            //MAJOR version is higher, we can update
            return true;
        }
        if ($lastVersionArray[0] >= $actualVersionArray[0] && $lastVersionArray[1] > $actualVersionArray[1]) {
            //MAJOR version is equal or higher AND MINOR version is higher, we can update
            return true;
        }

        if ($lastVersionArray[1] >= $actualVersionArray[1] && $lastVersionArray[2] > $actualVersionArray[2]) {
            //MINOR version is equal or higher AND hotfix version is higher, we can update
            return true;
        }

        return false;
    }
}
