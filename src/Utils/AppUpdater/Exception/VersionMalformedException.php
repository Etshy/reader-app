<?php declare(strict_types=1);

namespace App\Utils\AppUpdater\Exception;

use Exception;

class VersionMalformedException extends Exception
{
}
