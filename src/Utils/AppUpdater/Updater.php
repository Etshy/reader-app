<?php declare(strict_types=1);

namespace App\Utils\AppUpdater;

use App\Utils\AppUpdater\Exception\SourceZipNotAvailableExeption;
use App\Utils\AppUpdater\Exception\VersionMalformedException;
use Etshy\Bundle\PhpZipBundle\Services\PhpZipInterface;
use Exception;
use Gitlab\Client;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class Updater
{
    protected UpdateChecker $updateChecker;
    protected Client $gitlabClient;
    protected HttpClientInterface $httpClient;
    protected string $rootPath;
    protected Filesystem $filesystem;
    protected ?OutputInterface $output = null;
    protected TranslatorInterface $translator;
    protected Downloader $downloader;
    protected PhpZipInterface $phpZip;

    public function __construct(
        UpdateChecker $updateChecker,
        Client $gitlabClient,
        HttpClientInterface $httpClient,
        string $rootPath,
        Filesystem $filesystem,
        TranslatorInterface $translator,
        Downloader $downloader,
        PhpZipInterface $phpZip,
    ) {
        $this->updateChecker = $updateChecker;
        $this->gitlabClient = $gitlabClient;
        $this->httpClient = $httpClient;
        $this->rootPath = $rootPath;
        $this->filesystem = $filesystem;
        $this->translator = $translator;
        $this->downloader = $downloader;
        $this->phpZip = $phpZip;
    }

    /**
     * @throws VersionMalformedException
     * @throws SourceZipNotAvailableExeption
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function update(): void
    {
        $releases = $this->gitlabClient->repositories()->releases(5088846);
        $pregSearch = preg_match('/\d+\.\d+\.\d+/', reset($releases)['name'], $matches);
        if ($pregSearch === false) {
            $this->log($this->translator->trans('appupdater.error.version_malformed'));
            throw new VersionMalformedException();
        }

        $lastVersion = $matches[0];
        if (false === $this->updateChecker->isUpdateAvailable($lastVersion)) {
            $this->log('no update available');

            return;
        }

        //TODO add a maintenance system (maintenance=true in .env file and RequestListener to detect that)

        $this->log('update available. Search for file...');

        $lastRelease = reset($releases);
        $sourceUrl = $this->searchSourceUrl($lastRelease);


        if (null === $sourceUrl) {
            $this->log('file not found');
            throw new SourceZipNotAvailableExeption();
        }
        $this->log('file found, Downloading file ...');

        $filePath = $this->downloader->downloadFile($sourceUrl, $lastRelease['name']);

        $this->log('File downloaded');

        $fileSystem = new Filesystem();
        $updateDir = $this->rootPath.'/update_tmp';
        $fileSystem->mkdir($updateDir);

        $this->log('temp dir created');

        //unzip file into temp folder
        $zip = $this->phpZip->createZip();
        $zip->openFile($filePath);
        $zip->extractTo($updateDir);

        $this->log('zip file extracted');

        //TODO move only needed folder: assets, src, config, src, template, translation
        // + files : composer.json|lock, package.json, package-lock.json, semantic.json, symfony.lock

        //move temp folder content into current folder (caution with env file)
        $fileSystem->mirror($updateDir.'/bookuto-v'.$lastVersion, $this->rootPath);

        $this->log('files moved to app rootPath');

        //TODO Regenrate .env (just update APP_ENV line for now)
        $this->generateEnv($lastVersion);

        //TODO check for new values in env.dist file to add in .env ?
        // if new values, loop on them and ask for a value to put in ?

        $fileSystem->remove($updateDir);
        $fileSystem->remove($filePath);
        $this->log('temp files removed');

        //TODO launch a composer install

        //TODO launch a frontend install

        //TODO
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output): void
    {
        $this->output = $output;
    }

    private function searchSourceUrl(array $release)
    {
        foreach ($release['assets']['sources'] as $source) {
            if ($source['format'] === 'zip') {
                return $source['url'];
            }
        }

        return null;
    }

    private function log(string $message): void
    {
        //write to oputput if existing
        $this->output?->writeln($message);

        //TODO log ?
    }

    private function generateEnv($lastVersion): void
    {
        $reading = fopen('.env', 'rb');
        $writing = fopen('.env.tmp', 'wb');

        $replaced = false;

        while (!feof($reading)) {
            $line = fgets($reading);
            if (!is_string($line)) {
                continue;
            }
            if (stripos($line, 'VERSION') !== false) {
                $line = "VERSION=$lastVersion\n";
                $replaced = true;
            }
            fwrite($writing, $line);
        }
        fclose($reading);
        fclose($writing);

        // might as well not overwrite the file if we didn't replace anything
        if ($replaced) {
            rename('.env.tmp', '.env');
        } else {
            unlink('.env.tmp');
        }
    }
}
