<?php
declare(strict_types=1);

namespace App\View\Pagination;

use Pagerfanta\View\Template\SemanticUiTemplate;

/**
 * Class SemanticUIInvertedTemplate
 * @package App\View\Pagination
 */
class SemanticUIInvertedTemplate extends SemanticUiTemplate
{
    public function __construct()
    {
        parent::__construct();

        $cssContainerClass = $this->option('css_container_class');
        $cssContainerClass .= ' inverted ';

        $this->setOptions(['css_container_class' => $cssContainerClass]);
    }
}
