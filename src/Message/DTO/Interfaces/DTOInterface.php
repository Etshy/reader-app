<?php
declare(strict_types=1);

namespace App\Message\DTO\Interfaces;

/**
 * Interface DTOInterface
 * @package App\Message\DTO\Interfaces
 */
interface DTOInterface
{
    public function getId(): int|string;

    public function setId(int|string $id): void;
}
