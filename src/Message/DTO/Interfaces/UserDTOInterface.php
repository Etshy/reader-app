<?php
declare(strict_types=1);

namespace App\Message\DTO\Interfaces;

/**
 * Interface UserDTOInterface
 * @package App\Message\DTO\Interfaces
 */
interface UserDTOInterface extends DTOInterface
{
    public function getEmail(): string;

    public function setEmail(string $email): void;
}
