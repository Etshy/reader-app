<?php
declare(strict_types=1);

namespace App\Message\DTO;

use App\Message\DTO\Interfaces\DTOInterface;

/**
 * Class SeriesDTO
 * @package App\Message\DTO
 */
class SeriesDTO implements DTOInterface
{
    protected int|string $id;

    protected ?string $name;

    protected ?string $additionalNames;

    protected ?string $slug;

    public function getId(): int|string
    {
        return $this->id;
    }

    public function setId(int|string $id): void
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getAdditionalNames(): ?string
    {
        return $this->additionalNames;
    }

    public function setAdditionalNames(?string $additionalNames): void
    {
        $this->additionalNames = $additionalNames;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }
}
