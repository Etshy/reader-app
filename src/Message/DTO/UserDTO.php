<?php
declare(strict_types=1);

namespace App\Message\DTO;

use App\Message\DTO\Interfaces\UserDTOInterface;

/**
 * Class UserDTO
 * @package App\Message\DTO
 */
class UserDTO implements UserDTOInterface
{

    protected string|int $id;

    protected string $email;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(int|string $id): void
    {
        $this->id = $id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
}
