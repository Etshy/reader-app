<?php
declare(strict_types=1);

namespace App\Message\Handler;

use App\Exceptions\ChapterModelToDTOException;
use App\Message\ChapterNotificationMessage;
use App\Message\ChapterUserNotificationMessage;
use App\Message\DTO\SeriesDTO;
use App\Message\DTO\UserDTO;
use App\Message\Transformers\ModelDTOTransformer;
use App\Model\Interfaces\Model\FollowInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Service\FollowService;
use Exception;
use ReflectionException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;

/**
 * Class ChapterNotificationMessageHandler
 * @package App\Message\Handler
 */
class ChapterNotificationMessageHandler implements MessageHandlerInterface
{
    private FollowService $followService;

    private MessageBusInterface $bus;

    private ModelDTOTransformer $transformer;

    public function __construct(FollowService $followService, MessageBusInterface $bus, ModelDTOTransformer $transformer)
    {
        $this->followService = $followService;
        $this->bus = $bus;
        $this->transformer = $transformer;
    }

    public function __invoke(ChapterNotificationMessage $chapterNotificationMessage): void
    {
        try {
            $chapter = $chapterNotificationMessage->getChapter();
            $series = $chapter->getSeries();
            if (!$series instanceof SeriesDTO) {
                throw new Exception();
            }
            $follows = $this->followService->findBy([
                'series.id' => $series->getId(),
            ]);

            foreach ($follows as $follow) {
                if (!$follow instanceof FollowInterface) {
                    continue;
                }
                $user = $follow->getUser();
                if (!$user instanceof UserInterface) {
                    throw new Exception();
                }

                $userDTO = new UserDTO();
                $userDTO = $this->transformer->modelToDTO($user, $userDTO);

                if (!$userDTO instanceof UserDTO) {   //we don't have the expected DTO
                    throw new ChapterModelToDTOException();
                }
                //Create notification for $user and $chapter
                // As there could be a lot of users, create another Message and create each notification in a separate process
                $chapterUserNotificationMessage = new ChapterUserNotificationMessage();
                $chapterUserNotificationMessage->setChapter($chapter);
                $chapterUserNotificationMessage->setUser($userDTO);

                $this->bus->dispatch($chapterUserNotificationMessage);
            }
        } catch (ChapterModelToDTOException | ReflectionException | Throwable $e) {

        }
    }
}
