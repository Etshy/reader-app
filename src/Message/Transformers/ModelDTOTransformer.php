<?php
declare(strict_types=1);

namespace App\Message\Transformers;

use App\Exceptions\Message\DTONotTransformableException;
use App\Message\DTO\ChapterDTO;
use App\Message\DTO\Interfaces\DTOInterface;
use App\Message\DTO\SeriesDTO;
use App\Message\DTO\UserDTO;
use App\Model\Interfaces\Model\BaseModelInterface;
use App\Service\ChapterService;
use App\Service\SeriesService;
use App\Service\UserService;
use DateTime;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

/**
 * Class ModelDTOTransformer
 * @package App\Message\Transformers
 */
class ModelDTOTransformer
{
    private SeriesService $seriesService;
    private UserService $userService;
    private ChapterService $chapterService;
    private LoggerInterface $logger;

    /**
     * ModelDTOTransformer constructor.
     *
     * @param SeriesService $seriesService
     * @param UserService $userService
     * @param ChapterService $chapterService
     * @param LoggerInterface $logger
     */
    public function __construct(SeriesService $seriesService, UserService $userService, ChapterService $chapterService, LoggerInterface $logger)
    {
        $this->seriesService = $seriesService;
        $this->userService = $userService;
        $this->chapterService = $chapterService;
        $this->logger = $logger;
    }


    /**
     * @param BaseModelInterface $model
     * @param DTOInterface $DTO
     *
     * @return DTOInterface
     * @throws ReflectionException
     */
    public function modelToDTO(BaseModelInterface $model, DTOInterface $DTO): DTOInterface
    {
        $reflectionModel = new ReflectionClass($model);
        $reflectionDTO = new ReflectionClass($DTO);
        //get Properties from Model (Entity oif Document)
        $modelProperties = $reflectionModel->getProperties();
        foreach ($modelProperties as $property) {
            if (!$property instanceof ReflectionProperty) {
                continue;
            }

            $propertySetter = 'set'.ucfirst($property->getName());
            $propertyGetter = 'get'.ucfirst($property->getName());
            $propertyIsser = 'is'.ucfirst($property->getName());

            //Check if the DTO Class have the property and the setter
            if ($reflectionDTO->hasProperty($property->getName())
                && $reflectionDTO->hasMethod($propertySetter)
            ) {
                //Check if the model have a getProperty method
                if ($reflectionModel->hasMethod($propertyGetter)) {
                    if (is_object($model->$propertyGetter())
                        && !$model->$propertyGetter() instanceof DateTime
                    ) {
                        //the property is a class.
                        $subModel = $model->$propertyGetter();
                        $reflectionSubModel = new ReflectionClass($subModel);
                        $subDTOName = 'App\\Message\\DTO\\'.$reflectionSubModel->getParentClass()->getShortName().'DTO';
                        $subDTO = new $subDTOName();
                        $DTO->$propertySetter($this->modelToDTO($subModel, $subDTO));
                    } else {
                        //
                        $DTO->$propertySetter($model->$propertyGetter());
                    }
                } //Check if the model have a isProperty method (for bool property)
                elseif ($reflectionModel->hasMethod($propertyIsser)) {
                    $DTO->$propertySetter($model->$propertyIsser());
                }
            }
        }

        return $DTO;
    }

    /**
     * @param DTOInterface $DTO
     * @param BaseModelInterface $model
     *
     * @return BaseModelInterface
     * @throws ReflectionException
     */
    public function DTOToModel(DTOInterface $DTO, BaseModelInterface $model): BaseModelInterface
    {
        $reflectionModel = new ReflectionClass($model);
        $reflectionDTO = new ReflectionClass($DTO);

        //get Properties from Model (Entity oif Document)
        $modelProperties = $reflectionModel->getProperties();

        foreach ($modelProperties as $property) {
            if (!$property instanceof ReflectionProperty) {
                continue;
            }

            $propertySetter = 'set'.ucfirst($property->getName());
            $propertyGetter = 'get'.ucfirst($property->getName());
            $propertyIsser = 'is'.ucfirst($property->getName());

            //Check if the DTO Class have the property and the setter
            if ($reflectionDTO->hasProperty($property->getName())
                && $reflectionDTO->hasMethod($propertySetter)
            ) {
                //Check if the model have a getProperty method
                if ($reflectionModel->hasMethod($propertyGetter)) {
                    if (is_object($DTO->$propertyGetter())) {
                        //the property is a class.
                        $subDTO = $DTO->$propertyGetter();
                        if ($subDTO instanceof DTOInterface) {
                            try {
                                $subModel = $this->createModelInstanceFromDTO($subDTO);

                                $model->$propertySetter($this->DTOToModel($subDTO, $subModel));
                            } catch (DTONotTransformableException $e) {
                                $this->logger->error($e->getMessage());
                                continue;
                            }
                        }
                    } else {
                        $model->$propertySetter($DTO->$propertyGetter());
                    }
                } //Check if the model have a isProperty method (for bool property)
                elseif ($reflectionModel->hasMethod($propertyIsser)) {
                    $model->$propertySetter($DTO->$propertyIsser());
                }
            }
        }

        return $model;
    }

    /**
     * @throws DTONotTransformableException
     */
    private function createModelInstanceFromDTO(DTOInterface $DTO): BaseModelInterface
    {
        if (!$DTO->getId()) {
            throw new DTONotTransformableException('The given DTO doesn\'t have an id : '.get_class($DTO));
        }

        return match (true) {
            $DTO instanceof SeriesDTO => $this->seriesService->find($DTO->getId()),
            $DTO instanceof ChapterDTO => $this->chapterService->find($DTO->getId()),
            $DTO instanceof UserDTO => $this->userService->find($DTO->getId()),
            default => throw new DTONotTransformableException('The given DTO is not handled to transform into Model : '.get_class($DTO)),
        };
    }
}
