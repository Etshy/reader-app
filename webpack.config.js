// webpack.config.js
let Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where all compiled assets will be stored
    .setOutputPath('public/build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath((!Encore.isProduction()) ? '/rapp/build' : '/bookuto/build')

    // this is now needed so that your manifest.json keys are still `build/foo.js`
    // i.e. you won't need to change anything in your Symfony app
    .setManifestKeyPrefix('build')

    // will create public/build/main.js and public/build/main.css
    .addEntry('main', './assets/js/main.js')
    //Add entry if other js/css needed. first parameter is the generated filename.
    //require scss file in js. (if you addEntry for scss file only, it will create a js file with same name)
    .addEntry('reader', './assets/js/reader.js').addEntry('admin', './assets/js/admin.js').addEntry('public', './assets/js/public.js').addEntry('series_filters', './assets/js/series_filters.js')

    //file upload with dropzone
    .addEntry('edit-chapter', './assets/js/edit-chapter.js')

    //Admin chapter js
    .addEntry('admin-chapter', './assets/js/chapter.js').addEntry('admin-unfollow', './assets/js/backend/unfollow.js').addEntry('admin-mark_as_read', './assets/js/backend/mark_as_read.js')

    //addStyleEntry : for css file only
    //.addStyleEntry('public', './assets/css/public.scss')

    // allow sass/scss files to be processed
    .enableSassLoader()

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery().enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction()).splitEntryChunks().enableSingleRuntimeChunk().copyFiles({
  from: './assets/img',
  // optional target path, relative to the output dir
  //to: 'images/[path][name].[ext]',

  // if versioning is enabled, add the file hash too
  to: Encore.isProduction() ? 'img/[path][name].[hash:8].[ext]' : 'img/[path][name].[ext]',

  // only copy files matching this pattern
  pattern: /\.(png|jpg|jpeg|gif|ico|svg|webp)$/,
}).configureImageRule({
  filename: Encore.isProduction() ? 'img/[path][name].[hash:8][ext]' : 'img/[path][name][ext]',
}).configureBabelPresetEnv((config) => {
  config.useBuiltIns = 'usage';
  config.corejs = 3;
}).configureBabel((config) => {
  config.plugins.push('@babel/plugin-proposal-class-properties');
  config.plugins.push('@babel/plugin-proposal-private-methods');
})
;

// export the final configuration
module.exports = Encore.getWebpackConfig();