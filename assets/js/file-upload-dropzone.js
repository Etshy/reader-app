import ImageScrambler from './scramble-unscramble-image/image-scrambler';
import Dropzone from 'dropzone/dist/dropzone';

require('dropzone/dist/min/dropzone.min.css');

Dropzone.autoDiscover = false;

let chapterEditionHelper = document.getElementById('chapter_edition_helper');
let chapterId = chapterEditionHelper.dataset.chapter_id;
let seriesId = chapterEditionHelper.dataset.series_id;
let url = chapterEditionHelper.dataset.url;
let uploadingTrans = chapterEditionHelper.dataset.uploading_trans;
let uploadSuccessTrans = chapterEditionHelper.dataset.upload_success_trans;
let uploadFailedTrans = chapterEditionHelper.dataset.upload_failed_trans;
let deleteFileTrans = chapterEditionHelper.dataset.delete_file_trans;
let updatePageOrderUrl = chapterEditionHelper.dataset.update_page_order_url;
let deleteAllUrl = chapterEditionHelper.dataset.delete_all_url;
/**
 *
 * @type {Object}
 * @property {boolean} imageScrambled
 */
let settings = JSON.parse(chapterEditionHelper.dataset.settings);

let fileOrder = 1;

let filesUploaded = 0;
let filesToUpload = 0;

// Get the template HTML and remove it from the document
let previewNode = document.querySelector('#template');
previewNode.id = '';
let previewTemplate = previewNode.parentElement.innerHTML;
previewNode.parentNode.removeChild(previewNode);

let filesToAddToUpload = [];

let myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
  url: url, // Set the url
  thumbnailWidth: 150,
  thumbnailHeight: 80,
  acceptedFiles: 'image/*',
  previewTemplate: previewTemplate,
  autoQueue: false, // Make sure the files aren't queued until manually added
  previewsContainer: '#previews', // Define the container to display the previews
  clickable: '.fileinput-button', // Define the element that should be used as click trigger to select files.
});

let chapterIdNumericOnly = chapterId.replaceAll(/\D/g, '');
chapterIdNumericOnly = chapterIdNumericOnly.substr(-2, 2);
let scramblerOptions = {
  seed: chapterId,
  sliceSize: chapterIdNumericOnly,
};

myDropzone.on('addedfile', function(file) {
  if (file.id) {
    //existing file
    $(file.previewTemplate).remove();
    file.previewTemplate = $('[data-old-order=' + file.id + ']')[0];
    file.status = Dropzone.SUCCESS;
    myDropzone.files.push(file);
  } else {
    // Hookup the start button
    file.previewElement.querySelector('.start').onclick = function() {
      myDropzone.enqueueFile(file);
    };

    fileOrder = myDropzone.files.length;
    $(file.previewElement).data('order', fileOrder);
    $(file.previewTemplate).find('.file-progress').progress({
      percent: 0,
      text: {
        active: uploadingTrans,
        success: uploadSuccessTrans,
        error: uploadFailedTrans,
      },
    });
  }

  ++filesToUpload;
});

myDropzone.on('thumbnail', function(file) {
  if (settings.imageScrambled) {
    let image = new Image();
    image.crossOrigin = 'anonymous';
    image.src = file.dataURL;
    image.onload = function() {
      let imageScrambler = new ImageScrambler(scramblerOptions);
      let canvas = imageScrambler.scrambleImage(image);
      canvas.toBlob(blob => filesToAddToUpload[file.name] = blob);
    };
  }
});

// Update the total progress bar
// noinspection JSUnresolvedFunction
/*myDropzone.on('totaluploadprogress',
    function(progress /!*byteSent, totalByteSent*!/) {
      let $total = $('#total-progress');
      $total.removeClass('hide');
      $total.progress('set percent', progress);
    },
);*/

// noinspection JSUnusedLocalSymbols,JSUnresolvedFunction
myDropzone.on('uploadprogress', function(file, progress /*byteSent*/) {
  $(file.previewTemplate).find('.file-progress').removeClass('hide');

  $(file.previewTemplate).
      find('.file-progress').
      progress('set percent', progress);

});

myDropzone.on('sending', function(file, xhr, formData) {
  // Show the total progress bar when upload starts
  //$("#total-progress").removeClass('hide');
  // And disable the start button
  file.previewElement.querySelector('.start').
      setAttribute('disabled', 'disabled');

  let order = $(file.previewElement).data('order');
  formData.append('order', order);
  formData.append('chapterId', chapterId);
  formData.append('seriesId', seriesId);
  if (file.name in filesToAddToUpload) {
    let scrambledImage = new File([filesToAddToUpload[file.name]], file.name, {type: file.type});
    formData.append('imageScrambled', scrambledImage);
  }
});

myDropzone.on('error', function(file) {
  $(file.previewTemplate).find('.file-progress').progress('set error');
  $('#total-progress').progress('set error');
});

myDropzone.on('success', function(file, response) {
  $(file.previewTemplate).find('.file-progress').progress('set success');

  let data = response;
  $(file.previewElement).addClass('uploaded');
  $(file.previewElement).find('.start').remove();
  $(file.previewElement).find('.cancel').remove();
  let $deleteBtn = $('<button/>');
  $deleteBtn.attr('data-dz-remove', '');
  $deleteBtn.addClass('ui button red delete');
  $deleteBtn.attr('data-delete-url', data.delete_url);
  $deleteBtn.attr('data-delete-method', data.delete_method);
  let $icon = $('<i/>');
  $icon.addClass('ui icon delete');
  $deleteBtn.append($icon);
  let $span = $('<span/>');
  $span.text(deleteFileTrans);
  $deleteBtn.append($span);

  $(file.previewElement).find('.file-actions').append($deleteBtn);
  fileOrder++;
  $(file.previewElement).attr('data-old-order', fileOrder);

  // calcule progress
  ++filesUploaded;
  let progress = (100 * filesUploaded) / filesToUpload;
  let $total = $('#total-progress');
  $total.removeClass('hide');
  $total.progress('set percent', progress);
});

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on('queuecomplete', function(/*progress*/) {
  $('#total-progress').addClass('hide');
  filesUploaded = 0;
  filesToUpload = 0;
  //$("#previews").find('.start').removeAttr("disabled");
  //$("#total-progress").progress('set success');
});

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
document.querySelector('#actions .start').onclick = function() {
  myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
};
document.querySelector('#actions .cancel').onclick = function() {
  myDropzone.removeAllFiles(true);
  //$("#total-progress").addClass('hide');
};

//jQuery
$(function() {
  let $previews = $('#previews');

  $('[name="teams"]').attr('name', 'teams[]');

  $('#total-progress').progress({
    percent: 0,
    text: {
      active: uploadingTrans,
      success: uploadSuccessTrans,
      error: uploadFailedTrans,
    },
  });

  $previews.on('click', '[data-dz-remove]', function(ev) {
    ev.preventDefault();
    let deleteUrl = $(this).attr('data-delete-url');
    let deleteMethod = $(this).attr('data-delete-method');

    let _this = this;

    fileOrder = myDropzone.files.length;

    $.ajax({
      url: deleteUrl,
      type: deleteMethod,
      success: function(/*result*/) {
        $(_this).closest('.file-row').fadeOut(400, function() {
          updateImageOrder();
        });
      },
    });
  });

  $('#delete_all').on('click', function(evt) {
    evt.preventDefault();

    $.ajax({
      url: deleteAllUrl,
      type: 'DELETE',
      success: function(/*result*/) {
        // Do something with the result (hide a loader)?
        $previews.empty();
        myDropzone.files = [];
        fileOrder = 1;
      },
    });
  });

  let elems = $previews.find('.uploaded'), count = elems.length;
  elems.each(function() {
    // Create the mock file:
    let mockFile = {
      name: $(this).find('.name').text(),
      id: $(this).attr('data-old-order'),
    };

    // Call the default addedfile event handler
    myDropzone.emit('addedfile', mockFile);
    if (!--count) {
      // Make sure that there is no progress bar, etc...
      myDropzone.emit('complete', mockFile);
    }

  });

  $previews.sortable({
    items: '.file-row',
    cursor: 'move',
    opacity: 0.5,
    containment: '#previews',
    distance: 20,
    tolerance: 'pointer',
    stop: function() {
      updateImageOrder();
    },
  });

  $previews.on('click', '.preview', function(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    if ($(this).hasClass('zoom-image')) {
      $(this).children('.preview-full-screen').hide();
      $(this).removeClass('zoom-image');
    } else {
      if ($(this).children('.preview-full-screen').length === 0) {
        let $previewFullScreen = $('<div/>');
        $previewFullScreen.addClass('preview-full-screen');
        let srcLarge = $(this).data('src-large');
        // noinspection HtmlRequiredAltAttribute
        let $imgLarg = $('<img/>');
        $imgLarg.addClass('page page-large');
        $imgLarg.attr('src', srcLarge);
        $imgLarg.appendTo($previewFullScreen);
        $previewFullScreen.appendTo($(this));
      } else {
        $(this).children('.preview-full-screen').show();
      }

      $(this).addClass('zoom-image');
    }

  });

  function updateImageOrder() {

    let queue = myDropzone.files;
    let newQueue = [];
    let fileOrder = 1;
    let dataOrderUpdate = [];
    $('#previews .file-row:visible').each(function(count, el) {
      let name = $(el).find('[data-dz-name]').text();

      queue.forEach(function(file) {
        if (file.name === name) {
          newQueue.push(file);
          $(file.previewTemplate).data('order', fileOrder);
          if ($(file.previewTemplate).data('old-order')) {
            dataOrderUpdate.push({
              'oldOrder': $(file.previewTemplate).data('old-order'),
              'order': fileOrder,
            });
            $(el).closest('.file-row').data('old-order', fileOrder);
          }

          let $removeButton = $(file.previewTemplate).find('[data-dz-remove]');
          if ($removeButton.data('delete-url')) {
            let oldUrl = $removeButton.data('delete-url');
            let urlArray = oldUrl.split('/');
            urlArray[urlArray.length - 1] = fileOrder;
            let newUrl = urlArray.join('/');
            $removeButton.attr('data-delete-url', newUrl);
          }

          fileOrder++;
        }
      });
    });
    myDropzone.files = newQueue;
    let data = {
      'data': dataOrderUpdate,
    };
    //Ajax update pages order
    $.ajax({
      url: updatePageOrderUrl,
      type: 'PUT',
      data: data,
      success: function(/*result*/) {
        let queue = myDropzone.files;
        let fileOrder = 1;
        queue.forEach(function(file) {
          $(file.previewTemplate).data('order', fileOrder);
          fileOrder++;
        });
      },
    });
  }
});