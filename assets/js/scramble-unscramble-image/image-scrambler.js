import _ from 'lodash';
import shuffleSeed from 'shuffle-seed';

export default class ImageScrambler {
  _defaults = {
    sliceSize: 5,
    buffer: false,
  };
  _options = {};

  constructor(options) {
    this._options = _.extend({}, this._defaults, options);
  }

  /**
   * @param img
   * @returns {HTMLCanvasElement}
   */
  scrambleImage(img) {
    return this.#scrambleImage(img);
  }

  unscrambleImage(img) {
    return this.#scrambleImage(img, true);
  }

  #createCanvas(img) {
    let canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    return canvas;
  }

  #getSliceWidth(slicePosition, img) {
    let substract;
    if (parseInt(slicePosition.x) + parseInt(this._options.sliceSize) <= parseInt(img.width)) {
      substract = 0;
    } else {
      substract = (parseInt(slicePosition.x) + parseInt(this._options.sliceSize)) - parseInt(img.width);
    }
    return (parseInt(this._options.sliceSize) - parseInt(substract));
  }

  #getSliceHeight(slicePosition, img) {
    let substract;
    if (parseInt(slicePosition.y) + parseInt(this._options.sliceSize) <= parseInt(img.height)) {
      substract = 0;
    } else {
      substract = (parseInt(slicePosition.y) + parseInt(this._options.sliceSize)) - parseInt(img.height);
    }
    return (parseInt(this._options.sliceSize) - parseInt(substract));
  }

  #getGroup(slices) {
    let group = {};
    group.slices = slices.length;
    group.cols = this.#getColsInGroup(slices);
    group.rows = slices.length / group.cols;
    group.width = slices[0].width * group.cols;
    group.height = slices[0].height * group.rows;
    group.x = slices[0].pos.x;
    group.y = slices[0].pos.y;
    return group;
  }

  #getColsInGroup(slices) {
    if (slices.length === 1) {
      return 1;
    }
    let t = 'init';
    let i;
    for (i = 0; i < slices.length; i++) {
      if (t === 'init') {
        t = slices[i].pos.y;
      }
      if (t !== slices[i].pos.y) {
        //Return the index once we change line (y) - means we have the  number of columns
        return i;
      }
    }
    //if nout found in the for, we return last value of i - happens with the last row
    return i;
  }

  /**
   * return the total number of slices
   * @param sizeImg
   * @returns {number}
   */
  #getTotalSlices(sizeImg) {
    return Math.ceil(sizeImg.width / this._options.sliceSize) * Math.ceil(sizeImg.height / this._options.sliceSize);
  }

  /**
   * get Slices keyed by size
   * @param totalSlices
   * @param img
   * @returns {{}}
   */
  #getSlicesBySize(totalSlices, img) {
    const root = this;
    let slicesBySize = {};
    _.each(new Array(totalSlices), function(e, i) {
      let s = root.#getSlice(i, img);
      if (!slicesBySize[s.size]) {
        slicesBySize[s.size] = [];
      }
      slicesBySize[s.size].push(s);
    });
    return slicesBySize;
  }

  /**
   * return the chunk info (pos + size) for a given part of the image
   * @param part
   * @param img
   * @returns {{}}
   */
  #getSlice(part, img) {
    let slice = {};
    let pos = this.#getSlicePosition(part, img);
    slice.pos = pos;
    //Calculate the size of the slice
    //If the position + sliceSize is greater than image size we substract what exceeds it.
    slice.width = this.#getSliceWidth(pos, img);
    slice.height = this.#getSliceHeight(pos, img);
    slice.size = slice.width + '-' + slice.height;
    return slice;
  }

  /**
   * return the position of a given chunkId
   * @param part
   * @param sizeImg
   * @returns {{x: number, y: number}}
   */
  #getSlicePosition(part, sizeImg) {
    let verticalSlices = Math.ceil(sizeImg.width / this._options.sliceSize);

    let row = parseInt((part / verticalSlices).toString());
    let col = part - row * verticalSlices;
    return {
      x: Math.floor(col * this._options.sliceSize),
      y: Math.floor(row * this._options.sliceSize),
    };
  }

  #scrambleImage(img, unscramble = false) {
    if (!(img instanceof HTMLImageElement)) {
      throw 'img is not an Image';
    }

    let root = this;

    let totalSlices = this.#getTotalSlices(img);
    let slicesBySize;

    slicesBySize = this.#getSlicesBySize(totalSlices, img);

    let canvas = this.#createCanvas(img);
    let ctx = canvas.getContext('2d');

    //loop "group" of chunk - key is the size's info (width and height of slices) and slices is an array of slices corresponding to the slice sizes
    _.each(slicesBySize, function(slices) {

      let group = root.#getGroup(slices);

      let shuffleInd = [];
      //Add all the slices in the shuffleInd array
      for (let i = 0; i < slices.length; i++) {
        shuffleInd.push(i);
      }

      //shuffle the slices in the shuffleInd array
      shuffleInd = shuffleSeed.shuffle(shuffleInd, root._options.seed);

      //loop the slices
      _.each(slices, function(slice, key) {
        //for each slice get its new and original position
        let pos;
        let srcPos;
        if (unscramble) {
          srcPos = root.#getSlicePosition(shuffleInd[key], group);
          pos = root.#getSlicePosition(key, group);
        } else {
          pos = root.#getSlicePosition(shuffleInd[key], group);
          srcPos = root.#getSlicePosition(key, group);
        }

        //move slice in img canva from original position to new position
        ctx.drawImage(img,
            group.x + srcPos.x, group.y + srcPos.y, Number(slice.width), Number(slice.height),
            group.x + pos.x, group.y + pos.y, Number(slice.width), Number(slice.height));
      });
    });

    return canvas;
  }
}