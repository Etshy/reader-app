import NotificationViewer from './Notification/NotificationViewer';

$(function() {

  if (!('content' in document.createElement('template'))) {
    return false;
  }

  if (!document.getElementById('notification_helper')) {
    //not connected
    return false;
  }

  let $notificationHelper = document.getElementById('notification_helper');
  /** @type {string} */
  let chapterLabelTranslated = $notificationHelper.dataset.chapter_label_translated
  /** @type {string} */
  let renderUrl = $notificationHelper.dataset.render_url
  /** @type {string} */
  let notificationUrl = $notificationHelper.dataset.notification_url
  /** @type {string} */
  let chapterUrl = $notificationHelper.dataset.chapter_url
  /** @type {string} */
  let sseNotificationUrl = $notificationHelper.dataset.sse_notification_url
  /** @type {string} */
  let latestsNotificationsUrl = $notificationHelper.dataset.latests_notifications_url

  let options = {
    chapterLabelTranslated: chapterLabelTranslated,
    renderUrl: renderUrl,
    notificationUrl: notificationUrl,
    chapterUrl: chapterUrl,
    sseNotificationUrl: sseNotificationUrl,
    latestsNotificationsUrl: latestsNotificationsUrl
  }

  let notificationViewer = new NotificationViewer(options);
  notificationViewer.init();
});