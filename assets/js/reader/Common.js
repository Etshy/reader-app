import ImageScrambler from '../scramble-unscramble-image/image-scrambler';

class Common
{

  static READER_DIRECTION_LTR_VALUE = "ltr";
  static READER_DIRECTION_RTL_VALUE = "rtl";

  static READER_MODE_CLASSIC_VALUE = 'classic';
  static READER_MODE_WEBTOON_VALUE = 'webtoon';

  static READER_EVENT_LAST_PAGE = 'reader.event.last_page';

  _element = document.getElementById('classic_reader');
  _elementReaderContainer = document.getElementById('reader_container');
  _elementReaderFormChapter = document.getElementById('reader_form_chapter');
  _elementReaderFormPage = document.getElementById('reader_form_page');
  _elementPreviousPage = document.getElementById('prev_page');
  _elementNextPage = document.getElementById('next_page');
  _elementSettingsForm = document.getElementById('settings_form');
  _elementToggleSidebar = document.getElementById('sidebar_toggle');
  _elementSidebar = document.getElementById('sidebar_menu');


  _readerMode = 'READER_MODE_CLASSIC_VALUE';
  _readerDirection = 'rtl';

  _language;
  currentPage = 1;
  _currentNumberImageLoading = 0;
  _baseReaderUrl;
  _baseUrl;
  _series;

  _UnscramblerOptions;
  _ImageScrambler;

  _pageLoading = [];

  constructor(options) {

    if (options['element']) {
      this._element = options['element'];
    }
    if (options['readerMode']) {
      this._readerMode = options['readerMode'];
    }
    if (options['readerDirection']) {
      this._readerDirection = options['readerDirection'];
    }
    if (options['currentPage']) {
      this.currentPage = options['currentPage'];
    }
    if (options['isScrambled'] && !options['UnscramblerOptions']) {
      throw new Error('UnscramblerOptions Required');
    }
    this._UnscramblerOptions = options['UnscramblerOptions'];
    if(!options['language']) {
      throw new Error('language Required');
    }
    if(!options['baseReaderUrl']) {
      throw new Error('baseReaderUrl Required');
    }
    if(!options['baseUrl']) {
      throw new Error('baseUrl Required');
    }
    if(!options['series']) {
      throw new Error('series Required');
    }

    this._language = options['language'];
    this._baseReaderUrl = options['baseReaderUrl'];
    this._baseUrl = options['baseUrl'];
    this._series = options['series'];

  }

  /**
   * Can't use arrow function here
   */
  init() {
    window.stateChangeIsLocal = true;

    this._ImageScrambler = new ImageScrambler(this._UnscramblerOptions);

    //register events

    window.onpopstate = this._onStateChange.bind(this);
    this._elementReaderFormChapter.addEventListener('change', this._onChapterSelectChange.bind(this));
    this._elementSettingsForm.addEventListener('submit', this._onSubmitSettingsForm.bind(this));
    this._elementToggleSidebar.addEventListener('click', this._onClickToggleSidebar.bind(this));

    let $pageDiv = document.getElementById('page_'+this.currentPage);
    $pageDiv.classList.remove('hide');

    this._updateUrl();
  }

  changeChapter($chapterElement) {
    let chapterUrl = this._baseUrl + 'reader/' + this._series.slug + '/';
    chapterUrl += $chapterElement.dataset.number;
    chapterUrl += '/'+this._language;
    chapterUrl += '/page/1';
    //For now, basic redirection to chapter
    location.assign(chapterUrl);
  }

  _updateUrl() {
    let fullUrl = this._getFullReaderUrlWithoutPage();
    history.pushState({page: this.currentPage}, window.document.title, fullUrl + '/' + this.currentPage);
  }

  _getFullReaderUrlWithoutPage() {
    let url;
    //create new URL object
    url = new URL(this._baseReaderUrl);
    let pathname = url.pathname;
    //delete teh last part of the path (page number)
    let pathnameItems = pathname.split('/');
    pathnameItems.pop();
    //rejoin the path
    pathname = pathnameItems.join('/');
    return url.origin + pathname;
  }

  _scrollToTop() {
    window.scrollTo(0, 0);
  }

  checkIfPreviousChapter() {
    let currentChap = this._elementReaderFormChapter.value;
    let $currentChapOption = document.querySelectorAll('#reader_form_chapter>option[value=\'' + currentChap + '\']')[0];

    return $currentChapOption.previousElementSibling !== null;
  }

  checkIfPreviousPage() {
    let $PageOption = document.querySelectorAll('#reader_form_page>option[value=\'' + (this.currentPage - 1) + '\']')[0];
    return $PageOption.previousElementSibling !== null;
  }

  checkIfNextChapter() {
    let currentChap = this._elementReaderFormChapter.value;
    let $currentChapOption = document.querySelectorAll('#reader_form_chapter>option[value=\'' + currentChap + '\']')[0];
    return $currentChapOption.nextElementSibling !== null;
  }

  checkIfNextPage() {
    let $PageOption = document.querySelectorAll('#reader_form_page>option[value=\'' + (this.currentPage - 1) + '\']')[0];
    return $PageOption.nextElementSibling !== null;
  }

  _prevChapter() {
    window.stateChangeIsLocal = false;
    //location.href
    let currentChap = this._elementReaderFormChapter.value;
    let $currentChapOption = document.querySelector('#reader_form_chapter>option[value=\'' + currentChap + '\']');
    if ($currentChapOption.previousElementSibling !== null) {
      //there is a previous chapter
      let $prevChapterElem = $currentChapOption.previousElementSibling;
      this.changeChapter($prevChapterElem);
      //return true;
    }
  }

  _nextChapter() {
    window.stateChangeIsLocal = false;
    //location.href
    let currentChap = this._elementReaderFormChapter.value;
    let $currentChapOption = document.querySelector('#reader_form_chapter>option[value=\'' + currentChap + '\']');
    if ($currentChapOption.nextElementSibling !== null) {
      //there is a following chapter
      let $nextChapterElem = $currentChapOption.nextElementSibling;
      this.changeChapter($nextChapterElem);
    } else {
      //no chapter, dispatch event
      let Event = new CustomEvent(Common.READER_EVENT_LAST_PAGE, {});
      document.dispatchEvent(Event);
    }
  }

  _toggleSidebar() {
    this._elementSidebar.classList.toggle('open');
    document.body.classList.toggle('sidebar-opened')
  }

  _updateMenuVisibility(){
    if (this.checkIfPreviousChapter() || this.checkIfPreviousPage()) {
      this._elementPreviousPage.classList.remove('hide')
    }
    else {
      this._elementPreviousPage.classList.add('hide')
    }
    if (this.checkIfNextChapter() || this.checkIfNextPage()) {
      this._elementNextPage.classList.remove('hide')
    }
    else {
      this._elementNextPage.classList.remove('hide')
    }
  }

  /**
   * EVENTS
   */

  _onStateChange() {
    if (window.stateChangeIsLocal) {
      let State = history.state;
      if (State !== null && typeof State !== 'undefined') {
        this.currentPage = State.page;
      } else {
        this.currentPage = 1;
      }
      if (isNaN(this.currentPage) || typeof this.currentPage === 'undefined') {
        this.currentPage = 1;
      }
      //Call the subClass methods
      super._selectPage(this.currentPage);

      this._updateMenuVisibility();

      let event = new CustomEvent('ReaderOnStateChange', {detail: {Reader: this}});
      document.dispatchEvent(event);
    }
  }

  _onChapterSelectChange() {
    let chapValue = this._elementReaderFormChapter.value;
    let $selectedOption = this._elementReaderFormChapter.querySelector('option[value="' + chapValue + '"]');
    this.changeChapter($selectedOption);
  }

  _onSubmitSettingsForm(event) {
    event.preventDefault();
    let $form = event.currentTarget;
    let formData = new FormData($form);
    let url = $form.getAttribute('action');

    if (this._elementSettingsForm.querySelectorAll('.message').length > 0) {
      let messageElements = this._elementSettingsForm.querySelectorAll('.message');
      messageElements.forEach(function(element) {
        element.remove()
      })
    }

    let xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.addEventListener('load', this._onSaveSettingsFormSuccess.bind(this));
    xhr.addEventListener('error', this._onSaveSettingsFormError.bind(this));
    xhr.send(formData);
  }

  _onSaveSettingsFormSuccess(event) {
    let data = JSON.parse(event.target.response);
    if (data.message) {
      let $message = document.createElement('div');
      let classesToAdd;
      if (data.success) {
        classesToAdd = ['ui', 'success', 'message'];
        $message.classList.add(...classesToAdd);
      } else {
        classesToAdd = ['ui', 'negative', 'message'];
        $message.classList.add(...classesToAdd);
      }
      let $icon = document.createElement('i');
      classesToAdd = ['close', 'icon'];
      $icon.classList.add(...classesToAdd);
      $message.prepend($icon);
      let $p = document.createElement('p');
      $p.innerText = data.message;
      $p.textContent = data.message;
      $message.append($p);
      this._elementSettingsForm.insertAdjacentElement('beforebegin', $message);
    }

    if (data.success) {
      setTimeout(function() {
        location.reload();
      }, 3000);
    }
  }

  _onSaveSettingsFormError(event) {
    let data = JSON.parse(event.target.response);
    if (data.message) {
      let $message = document.createElement('div');
      let classesToAdd = ['ui', 'negative', 'message'];
      $message.classList.add(...classesToAdd);
      let $icon = document.createElement('i');
      classesToAdd = ['close', 'icon'];
      $icon.classList.add(...classesToAdd);
      $message.prepend($icon);
      let $p = document.createElement('p');
      $p.innerText = data.message;
      $p.textContent = data.message;
      $message.append($p);
      this._elementSettingsForm.insertAdjacentElement('beforebegin', $message);
    }
  }

  _onClickToggleSidebar() {
    this._toggleSidebar();
  }

}

export {Common}