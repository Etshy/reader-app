import {Common} from './Common';

class WebtoonReader extends Common {

  _isMobile=false;

  constructor(options) {
    super(options);

    if (options['isMobile']) {
      this._isMobile = options['isMobile'];
    }
  }

  init() {
    super.init();

    if (!this._isMobile) {
      this.currentPage = 1;
      this._elementReaderFormPage.addEventListener('change', this._readerFormPageOnChange.bind(this));
      document.addEventListener('scroll', this._onScroll.bind(this));
    }

    this._lazyLoadImages();

    if (!this._isMobile) {
      if (this.currentPage > 1) {
        let $pageDiv = document.getElementById('page_' + this.currentPage);
        $pageDiv.scrollIntoView();
      }
    }
  }

  _lazyLoadImages = () => {
    this._loadImage(this.currentPage, true).then(() => {
      //load others pages
      this._currentNumberImageLoading--;
    });
  };

  async _loadImage(pageNumber, lazyLoad = false) {

    let pageNumberToLoad = pageNumber;
    let $pageDiv = document.getElementById('page_' + pageNumberToLoad);

    if (typeof ($pageDiv) === 'undefined' || $pageDiv === null) {
      return false;
    }

    //if page already load
    if ($pageDiv.classList.contains('loaded')) {
      return false;
    }

    this._currentNumberImageLoading++;
    this._pageLoading.push(pageNumberToLoad);

    let pageUrl = $pageDiv.dataset.image;
    let img = new Image();
    img.src = pageUrl;
    //img.loading = "lazy";
    img.decode().then(() => {

      let canvas;
      if ($pageDiv.dataset.isScrambled !== undefined) {
        canvas = this._ImageScrambler.unscrambleImage(img);
      } else {
        canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0, img.width, img.height);
      }
      $pageDiv.innerHTML = '';
      $pageDiv.append(canvas);
      let $wallDiv = document.createElement('div');
      $pageDiv.prepend($wallDiv);
      $wallDiv.classList.add('wall');
      $pageDiv.classList.add('loaded');
      $pageDiv.classList.remove('loading');

      if (lazyLoad) {
        let nextPageNumber = pageNumberToLoad + 1;
        this._loadImage(nextPageNumber, lazyLoad).then(() => {
          this._currentNumberImageLoading--;
        });
        let prevPageNumber = pageNumberToLoad - 1;
        this._loadImage(prevPageNumber, lazyLoad).then(() => {
          this._currentNumberImageLoading--;
        });
      }
    }).catch((e) => {
      console.error(e);
    });
  }

  _selectPage(pageNumberToLoad) {
    this.currentPage = pageNumberToLoad;
    let $pageDiv = document.getElementById('page_' + pageNumberToLoad);
    $pageDiv.scrollIntoView();
  }

  _checkElementOnTop(elm) {
    let rect = elm.getBoundingClientRect();
    return rect.top < 100 && rect.top >= -1;
  }

  _readerFormPageOnChange(event) {
    let value = parseInt(event.currentTarget.value) + 1;
    if (value === this.currentPage) {
      return false;
    }
    this._selectPage(value);
  }

  _onScroll() {
    let elements = document.querySelectorAll('#webtoon_reader .page');
    for (let i = 0; i < elements.length; i++) {
      if (this._checkElementOnTop(elements[i])) {
        let id = elements[i].getAttribute('id');

        let idSplit = id.split('_');
        let page = Number(idSplit[1]);

        if (page === this.currentPage) {
          return false;
        }

        this.currentPage = page;

        this._updateUrl();

        //Emit event
        let event = new CustomEvent('ReaderOnScrollUpdateDropdown', {detail: {Reader: this}});
        document.dispatchEvent(event);

        return false;
      }
    }
  }

}

export {WebtoonReader}