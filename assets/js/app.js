//General JS/Jquery (semantic ui element activation and things)

function addLoadingBody(){
    let $body = $('body');
    $body.dimmer('show');
    let $loader = $('<div/>');
    $loader.addClass('ui active text loader');
    $loader.text("Loading...");
    $body.append($loader);
}
function removeLoadingBody(){
    let $body = $('body');
    let $loader = $body.children('.loader');
    $loader.remove();
    $body.dimmer('hide');
}
window.addLoadingBody = addLoadingBody;
window.removeLoadingBody = removeLoadingBody;

//jQuery
$(function(){

    /**
     * Semantic initialization
     */
    let $body = $('body');
    $body.dimmer({
        closable: false
    });
    //dropdown
    $('.ui.dropdown').dropdown();
    //Tabs
    $('.menu.tabular .item').tab();
    //form loading
    $('form [type=submit]').on('click', function(){
        $(this).closest('form').addClass('loading');
    });
    //fix for checkbox
    $("input[type=checkbox]").on('click', function () {
        if($(this).is(':checked')) {
            $(this).val(true);
        } else {
            $(this).val(false);
        }
    });

    //sidebar
    $("#sidebar_toggle").on('click', function () {
        $(".sidebar")
            .sidebar('setting', 'transition', 'push')
            .sidebar('toggle');
    });
    //prevent click on body dimmable active
    $(".dimmer").on('click', function(evt){
        evt.preventDefault();
        evt.stopPropagation();
    });
    //inline popup activation
    $(".inline-popup").popup({
        inline: true,
        on: 'click',
    });
    //Make a whole item with a link clickable
    $(".menu .item, .list .item").on('click', function () {
        if ($(this).children('a').length === 1) {
            //If we have a link inside
            location.href = $($(this).children('a')[0]).attr('href');
        }
    });
});